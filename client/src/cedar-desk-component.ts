import { Component } from 'elm-app';
import { CedarDeskApp } from './app';

/** A class purely for making the app() function be a CedarDeskApp. */
export class CedarDeskComponent extends Component {

	override get app(): CedarDeskApp {
		return super.app as CedarDeskApp;
	}
}
