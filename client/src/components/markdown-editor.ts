import { Component, Params } from 'elm-app';

/** The selection of the cursor. */
type Selection = {
	startNode: Node,
	startOffset: number,
	endNode: Node,
	endOffset: number,
	startIsAnchor: boolean
} | null;

/** The iteration used for moving through children, so that the for loops can be adjusted
 * as needed when the nodes it is iterating over change. */
class Iteration {
	childOffsets: number[] = [];
	lastTopLevelIndex: number = 0;
	setOffset(levelRelLength: number, offset: number): void {
		this.childOffsets[this.childOffsets.length + levelRelLength] = offset;
	}
	getOffset(levelRelLength: number): number {
		return this.childOffsets[this.childOffsets.length + levelRelLength];
	}
}

export class MarkdownEditor extends Component {

	constructor(params: Params) {
		super(params);

		// Create the mutation observer for keeping track of any changes done by the user.
		this.onMutation = this.onMutation.bind(this);
		this.mutationObserver = new MutationObserver(this.onMutation);
		this.startObserving();

		// Add any text.
		if (params.innerHtml !== '') {
			this.setText(params.innerHtml);
		}

		// If there's no readonly.
		if (params.attributes.get('readonly') === undefined) {

			// Override the paste to just paste plain text.
			this.root.addEventListener('copy', this.onCopy.bind(this) as EventListener);
			this.root.addEventListener('cut', this.onCut.bind(this) as EventListener);

			// Add an input changed callback.
			this.onChange = params.eventHandlers.get('change');
		}

		// It's readonly.
		else {
			this.root.removeAttribute('contenteditable');
		}
	}

	/** Gets the text in the editor. */
	getText(): string {
		return this.getTextFromNode(this.root);
	}

	private getTextFromNode(node: Node): string {
		if (node instanceof Text) {
			return node.data;
		}
		let text = [...node.childNodes]
			.map((child) => this.getTextFromNode(child))
			.reduce((text1, text2) => text1 + text2, '');
		if (node instanceof HTMLElement && ['P', 'LI', 'H1', 'H2', 'H3', 'BLOCKQUOTE'].includes(node.tagName)
			&& (node.tagName !== 'LI' || !['OL', 'UL'].includes(node.firstElementChild?.tagName ?? ''))
			&& (node.parentNode !== this.root || this.root.lastChild !== node)) {
			text += '\n';
		}
		return text;
	}

	/** Sets the text in the editor. */
	setText(text: string): void {

		// Clear out the existing text.
		const needsManualOnMutation = this.root.textContent === '' && text === '';
		this.root.innerHTML = '';

		// Turn the text into a bunch of paragraphs. Empty lines get br's.
		const textTokens = text.split('\n');
		for (let i = 0; i < textTokens.length; i++) {
			const textToken = textTokens[i];
			const p = document.createElement('p');
			if (textToken !== '') {
				p.innerText = textToken;
			}
			else {
				const br = document.createElement('br');
				p.append(br);
			}
			this.root.append(p);
		}

		// Clear the first mutation flag to signal that the next mutation shouldn't trigger the onChange event.
		// Setting '' on an existing '' doesn't trigger onMutation, so we need to manually do it.
		this.firstMutation = true;
		if (needsManualOnMutation) {
			this.onMutation([]);
		}
	}

	/** Starts observing the root. */
	private startObserving(): void {
		this.mutationObserver.observe(this.root, {
			subtree: true,
			childList: true,
			characterData: true,
			characterDataOldValue: true
		});
	}

	/** Stops observing the root. */
	private stopObserving(): void {
		this.mutationObserver.disconnect();
	}

	/** When the user copies some text. */
	private onCopy(event: ClipboardEvent): void {
		event.preventDefault();

		// Get the selection.
		const selection = document.getSelection();
		if (!selection) {
			return;
		}

		// Create a document fragment from the selection.
		const documentFragment = selection.getRangeAt(0).cloneContents();

		// Do a depth-first traversal, accumulating the text nodes, and adding \n where appropriate.
		let text = '';
		const dft = (node: Node): void => {
			node.childNodes.forEach((a) => {
				dft(a);
			});
			if (node instanceof Text) {
				text += node.textContent;
			}
			else if (node instanceof Element && ['P', 'LI', 'H1', 'H2', 'H3', 'BLOCKQUOTE'].includes(node.tagName)
				&& (node.tagName !== 'LI' || !['OL', 'UL'].includes(node.firstElementChild?.tagName ?? ''))
				&& (node.parentNode !== documentFragment || documentFragment.lastChild !== node)) {
				text += '\n';
			}
		};
		dft(documentFragment);

		// Save the text to the clipboard.
		event.clipboardData!.setData('text/plain', text);
	}

	/** When the user cuts some text. */
	private onCut(event: ClipboardEvent): void {

		// First do a copy.
		this.onCopy(event);

		// Delete the contents.
		const selection = document.getSelection();
		if (!selection) {
			return;
		}
		selection.deleteFromDocument();
	}

	private onMutation(mutations: MutationRecord[]): void {

		// Save any records still in the queue and stop observing the root so we can do DOM manipulation.
		const recordsInQueue = this.mutationObserver.takeRecords();
		mutations.push(...recordsInQueue);
		this.stopObserving();

		// console.log(mutations);

		// Save the selection.
		const selection = this.saveSelection();
		// console.log('Save Selection', selection);

		// Get every top level node that we need to deal with.
		const topLevelNodeSet = new Set<Node>();
		for (const mutation of mutations) {
			let topLevelNode: Node | null = null;
			if (mutation.type === 'childList') {
				if (mutation.addedNodes.length > 0) {
					topLevelNode = mutation.addedNodes[0];
				}
				else if (mutation.removedNodes.length > 0) {
					if (mutation.previousSibling) {
						topLevelNode = mutation.previousSibling;
					}
					else if (mutation.nextSibling) {
						topLevelNode = mutation.nextSibling;
					}
				}
			}
			else if (mutation.type === 'characterData') {
				topLevelNode = mutation.target;
			}
			while (topLevelNode && topLevelNode.parentNode! !== this.root) {
				topLevelNode = topLevelNode.parentNode!;
			}
			if (topLevelNode) {
				topLevelNodeSet.add(topLevelNode);
				if (mutation.type === 'childList' && mutation.removedNodes.length > 0 && mutation.previousSibling && topLevelNode.nextSibling) {
					topLevelNodeSet.add(topLevelNode.nextSibling);
				}
			}
		}

		// console.log(topLevelNodeSet);

		// Make a list of topLevelNodes, starting from the first to the last.
		let firstIndex: number = this.root.childNodes.length;
		let lastIndex: number = 0;
		for (const topLevelNode of topLevelNodeSet) {
			const childIndex = this.getIndexOfNode(topLevelNode);
			firstIndex = Math.min(firstIndex, childIndex);
			lastIndex = Math.max(lastIndex, childIndex);
		}

		// this.printNode('Before fix: ', this.root);

		// console.log('Checking indices:', firstIndex, lastIndex);

		// Go through each top level node and make sure they are all good.
		const iteration = new Iteration();
		iteration.childOffsets = [0];
		iteration.lastTopLevelIndex = lastIndex;
		for (let i = firstIndex; i <= iteration.lastTopLevelIndex && i < this.root.childNodes.length; i++) {
			const child = this.root.childNodes[i];
			this.checkNode(child, selection, iteration);

			// Adjust the index if we need to go over any child again, clearing the offset for the next run.
			if (iteration.getOffset(-1) < 0) {
				i = Math.max(-1, i + iteration.getOffset(-1));
				iteration.setOffset(-1, 0);
			}
		}

		// this.printNode('After fix: ', this.root);

		// Restore the selection.
		// console.log('Restore Selection', selection);
		this.restoreSelection(selection);

		// Resume observing the root.
		this.startObserving();

		// Call the changed callback if this isn't the first mutation (from initial load).
		// console.log('mutation', this.firstMutation);
		if (this.firstMutation) {
			this.firstMutation = false;
		}
		else {
			this.onChange?.();
		}
	}

	printNode(text: string, node: Node, level = 0): void {
		let indent = '';
		for (let i = 0; i < level; i++) {
			indent += '  ';
		}
		if (level === 0) {
			console.log(`---${text}`);
		}
		if (node === this.root) {
			console.log('ROOT');
		}
		else {
			if (indent !== '') {
				console.log(indent, node);
			}
			else {
				console.log(node);
			}
		}
		for (const child of node.childNodes) {
			this.printNode('', child, level + 1);
		}
	}

	/** Fixes a node. Return an offset to adjust the index of any child for loop.
	 * The child offsets are for adjusting the child iteration loops when things have been moved around.
	*/
	private checkNode(node: Node, selection: Selection, iteration: Iteration): void {

		// console.log('Visiting', node);

		// If the node is no longer in the root, do nothing.
		if (!this.root.contains(node)) {
			return;
		}

		// Check all of the children.
		iteration.childOffsets.push(0);
		for (let i = 0; i < node.childNodes.length; i++) {
			// console.log('ChildOffsets before: ', iteration.childOffsets);
			this.checkNode(node.childNodes[i], selection, iteration);
			// console.log('ChildOffsets after: ', iteration.childOffsets);

			// If any level but the last has a negative offset, we're going to be doing this over again anyway,
			// so just return.
			for (let j = 0; j < iteration.childOffsets.length - 1; j++) {
				if (iteration.childOffsets[j] < 0) {
					iteration.childOffsets.pop();
					return;
				}
			}

			if (iteration.getOffset(-1) < 0) {
				// Adjust the index if we need to go over any child again, clearing the offset for the next run.
				i = Math.max(-1, i + iteration.getOffset(-1));
				iteration.setOffset(-1, 0);
			}
		}
		iteration.childOffsets.pop();

		// // Print where we're at.
		// this.printNode('Checking', node);

		/// CLEAN UP

		if (this.cleanUpNode(node, selection, iteration)) {
			return;
		}

		/// REMOVING FORMATTING.

		// If the node is a heading and shouldn't be, make it a paragraph.
		if (node instanceof HTMLHeadingElement) {
			if (this.checkHeading(node, selection, iteration)) {
				return;
			}
		}

		// If the node is an li and shouldn't be, make it a paragraph.
		if (node instanceof HTMLLIElement) {
			if (this.checkLi(node, selection, iteration)) {
				return;
			}
		}

		// If the node is an hr and shouldn't be, fix it.
		if (node instanceof HTMLParagraphElement && node.classList.contains('hr')) {
			if (!node.textContent?.match(/^-{3,}$/)) {
				node.classList.remove('hr');
			}
		}

		// If the node is a paragraph inside of a blockquote and shouldn't be, make it a paragraph.
		if (node instanceof HTMLParagraphElement && node.parentElement instanceof HTMLQuoteElement) {
			if (this.checkBlockquote(node, selection, iteration)) {
				return;
			}
		}

		// If the node is a pre and shouldn't be, move out all of the children.
		if (node instanceof HTMLPreElement) {
			if (this.checkPre(node, selection, iteration)) {
				return;
			}
		}

		// If the node is an inline style and is no longer valid, remove the style.
		if (node instanceof HTMLElement) {
			if (this.checkRemoveInlineStyle(node, selection, iteration)) {
				return;
			}
		}

		// ADDING FORMATTING.

		// If it's a text and not already in an inline style, check to see if it can be converted.
		if (node instanceof Text) {
			if (this.checkAddInlineStyle(node, selection, iteration)) {
				return;
			}
		}

		// If the paragraph node starts with certain characters, change the node into its new tag.
		if (node instanceof HTMLParagraphElement && node.parentNode === this.root && node.textContent !== null) {
			if (node.textContent.startsWith('# ')) {
				// console.log('p -> h1');
				this.swapTag(node, 'h1', selection);
				iteration.setOffset(-1, -1);
			}
			else if (node.textContent.startsWith('## ')) {
				// console.log('p -> h2');
				this.swapTag(node, 'h2', selection);
				iteration.setOffset(-1, -1);
			}
			else if (node.textContent.startsWith('### ')) {
				// console.log('p -> h3');
				this.swapTag(node, 'h3', selection);
				iteration.setOffset(-1, -1);
			}
			else if (node.textContent?.match(/^-{3,}$/)) {
				// console.log('p -> hr');
				node.classList.add('hr');
			}
			else if (node.textContent.match(/^([ ][ ])*(\*|[0-9]+\.) /)) {
				// console.log('p -> (ol/ul)+ li');
				this.checkParagraphToLi(node, selection, iteration);
			}
			else if (node.textContent.match(/^> /)) {
				// console.log('p -> blockquote');
				this.convertParagraphToBlockquote(node, selection, iteration);
			}
			else if (node.textContent.match(/^```$/)) {
				// console.log('p -> pre');
				this.convertParagraphToPre(node, iteration);
			}
			return;
		}

		// Join any lists that need joining.
		if (node instanceof HTMLUListElement || node instanceof HTMLOListElement) {
			this.joinAdjacentLists(node, selection, iteration);
		}

		return;
	}

	/** Does clean up on various nodes to make things more proper for other conversions.
	 * Returns true if an offset was changed and we need to recheck things. */
	private cleanUpNode(node: Node, selection: Selection, iteration: Iteration): boolean {

		// Clear the attributes.
		if (node instanceof Element) {
			const attributeNames = node.getAttributeNames();
			for (const attributeName of attributeNames) {
				if (node instanceof HTMLAnchorElement && attributeName === 'href') {
					continue;
				}
				if (node instanceof HTMLParagraphElement && attributeName === 'class' && node.getAttribute(attributeName) === 'hr') {
					continue;
				}
				// console.log(`Removing attribute ${attributeName}`);
				node.removeAttribute(attributeName);
			}
		}

		// Remove any spans, moving their contents upward.
		if (node instanceof HTMLSpanElement) {
			// console.log(`Removing span, moving its children upward.`);
			if (node.parentElement === this.root) {
				iteration.lastTopLevelIndex += node.childNodes.length - 1;
			}
			node.after(...node.childNodes);
			this.removeNodeWithSelection(node, selection);
			iteration.setOffset(-1, -1);
			return true;
		}

		// Move any paragraph inside a paragraph upward before its parent.
		if (node instanceof HTMLParagraphElement && node.parentElement instanceof HTMLParagraphElement) {
			// console.log(`Moving paragraph inside a paragraph upward before its parent.`);
			const parent = node.parentElement;
			parent.before(node);
			if (parent.childNodes.length === 0) {
				if (parent.parentElement === this.root) {
					iteration.lastTopLevelIndex -= 1;
				}
				this.removeNodeWithSelection(parent, selection);
			}
			iteration.lastTopLevelIndex += 1;
			iteration.setOffset(-2, -2);
			return true;
		}

		if (node instanceof Text) {
			const match = node.data.match(/\r?\n/d);
			if (match) {
				// console.log('Removing \\r or \\n from text node');
				const nextNode = this.splitText(node, match.index!, selection);
				let topLevelNode: Element | Text = node;
				while (topLevelNode.parentElement !== this.root) {
					topLevelNode = topLevelNode.parentElement!;
				}
				if (nextNode === null) { // The \n was at the end.
					topLevelNode.after(new Text(''));
				}
				else if (nextNode === node) { // The \n was at the start.
					topLevelNode.after(nextNode);
					nextNode.before(new Text(''));
				}
				else { // The \n was in the middle.
					topLevelNode.after(nextNode);
				}
				if (nextNode) {
					nextNode.data = nextNode.data.slice(match[0].length);
					if (selection?.startNode === nextNode) {
						selection.startOffset -= 1;
					}
					if (selection?.endNode === nextNode) {
						selection.endOffset -= 1;
					}
				}
				iteration.childOffsets[0] -= 1;
				iteration.lastTopLevelIndex += 1;
				return true;
			}
		}

		// Make block level text and br nodes be inside paragraphs.
		if ((node instanceof Text || node instanceof HTMLBRElement) && (node.parentNode === this.root || node.parentElement?.tagName === 'BLOCKQUOTE')) {
			// console.log('Making block level text or br be inside p');

			// Create the new paragraph with the text child.
			const p = document.createElement('p');
			node.after(p);
			p.append(node);

			// Move the parent's child iteration back one to check appended nodes.
			iteration.setOffset(-1, -1);
			return true;
		}

		// If a text node is empty, remove it.
		if (node instanceof Text && node.data === '') {
			// console.log('Removing empty text node.');
			this.removeNodeWithSelection(node, selection);
			iteration.setOffset(-1, -1);
			if (node.parentElement === this.root) {
				iteration.lastTopLevelIndex -= 1;
			}
			return true;
		}

		// If this text node and the next text node, merge the next into this. Make sure to adjust the selection if needed.
		if (node instanceof Text && node.nextSibling instanceof Text) {
			// console.log('Combining text: ', node, node.nextSibling);
			if (selection) {
				if (selection.startNode === node.nextSibling) {
					selection.startNode = node;
					selection.startOffset += node.data.length;
				}
				if (selection.endNode === node.nextSibling) {
					selection.endNode = node;
					selection.endOffset += node.data.length;
				}
			}
			node.data += node.nextSibling.data;
			this.removeNodeWithSelection(node.nextSibling, selection);
			iteration.setOffset(-1, -1);
			if (node.parentElement === this.root) {
				iteration.lastTopLevelIndex -= 1;
			}
			return true;
		}

		// If this text node and the previous text node, merge this into the previous node. Make sure to adjust the selection if needed.
		if (node instanceof Text && node.previousSibling instanceof Text) {
			// console.log('Combining text: ', node.previousSibling, node);
			if (selection) {
				if (selection.startNode === node) {
					selection.startNode = node.previousSibling;
					selection.startOffset += node.previousSibling.data.length;
				}
				if (selection.endNode === node) {
					selection.endNode = node.previousSibling;
					selection.endOffset += node.previousSibling.data.length;
				}
			}
			node.previousSibling.data += node.data;
			this.removeNodeWithSelection(node, selection);
			iteration.setOffset(-1, -2);
			if (node.parentElement === this.root) {
				iteration.lastTopLevelIndex -= 1;
			}
			return true;
		}

		// Change any brs that are not the last children into paragraphs with a single br.
		if (node instanceof HTMLBRElement && node.nextSibling !== null) {
			// console.log('Moving non-last br into its own paragraph.');
			const p = document.createElement('p');
			node.after(p);
			p.append(node);
			iteration.setOffset(-1, -1);
			return true;
		}

		// Remove any brs are not the only children.
		if (node instanceof HTMLBRElement && node.previousSibling !== null) {
			// console.log('Removing unneeded last br.');
			this.removeNodeWithSelection(node, selection);
			iteration.setOffset(-1, -1);
			if (node.parentElement === this.root) {
				iteration.lastTopLevelIndex -= 1;
			}
			return true;
		}

		// Change any div into a paragraph.
		if (node instanceof HTMLDivElement) {
			// console.log('div -> p');
			this.swapTag(node, 'p', selection);
			iteration.setOffset(-1, -1);
			return true;
		}

		return false;
	}

	private checkRemoveInlineStyle(node: HTMLElement, selection: Selection, iteration: Iteration): boolean {
		if (!['B', 'I', 'CODE', 'A'].includes(node.tagName)) {
			return false;
		}

		const textNode = node.firstChild as Text;
		let match: RegExpMatchArray | null = null;
		if (node.tagName === 'B') {
			// First check to see if adjacent text nodes have a *, word, or \ in them. If they do, remove this.
			if ((node.previousSibling instanceof Text && node.previousSibling.data.match(/[\\*\w]$/))
				|| (node.nextSibling instanceof Text && node.nextSibling.data.match(/^[*\w]/))) {
				match = null;
			}
			else {
				match = textNode.data.match(/(?<![\\*\w])(\*\*)(?! )[^*]+?(?<! )(\*\*)(?![*\w])/d);
			}
		}
		else if (node.tagName === 'I') {
			// First check to see if adjacent text nodes have a *, word, or \ in them. If they do, remove this.
			if ((node.previousSibling instanceof Text && node.previousSibling.data.match(/[\\*\w]$/))
				|| (node.nextSibling instanceof Text && node.nextSibling.data.match(/^[*\w]/))) {
				match = null;
			}
			else {
				match = textNode.data.match(/(?<![\\*\w])(\*)(?! )[^*]+?(?<! )(\*)(?![*\w])/d);
			}
		}
		else if (node.tagName === 'CODE') {
			// First check to see if adjacent text nodes have a word or \ in them. If they do, remove this.
			if ((node.previousSibling instanceof Text && node.previousSibling.data.match(/[\\\w]$/))
				|| (node.nextSibling instanceof Text && node.nextSibling.data.match(/^\w/))) {
				match = null;
			}
			else {
				match = textNode.data.match(/(?<![\\\w])(`)(?! )[^`]+?(?<! )(`)(?!\w)/d);
			}
		}
		else if (node.tagName === 'A') {
			// First check to see if adjacent text nodes have a word or \ in them. If they do, remove this.
			if ((node.previousSibling instanceof Text && node.previousSibling.data.match(/[\\\w]$/))
				|| (node.nextSibling instanceof Text && node.nextSibling.data.match(/^\w/))) {
				match = null;
			}
			else {
				match = textNode.data.match(/(?<![\\\w])(<)(?! )[^>]+?(?<! )(>)(?!\w)/d);
			}
		}
		if (match) {
			const firstSplitIndex = match.indices![1][0];
			const secondSplitIndex = match.indices![2][1];
			if (secondSplitIndex !== undefined && secondSplitIndex < textNode.data.length) {
				const restOfTextNode = this.splitText(textNode, secondSplitIndex, selection)!;
				node.after(restOfTextNode);
				iteration.setOffset(-1, -1);
				return true;
			}
			if (firstSplitIndex !== undefined && firstSplitIndex > 0) {
				const restOfTextNode = this.splitText(textNode, firstSplitIndex, selection);
				if (restOfTextNode) {
					node.before(textNode);
					node.prepend(restOfTextNode);
					iteration.setOffset(-1, -2);
					return true;
				}
			}
		}
		else {
			// console.log(`${node.tagName} -> text`);
			node.before(textNode);
			this.removeNodeWithSelection(node, selection);
			iteration.setOffset(-1, -1);
			return true;
		}
		return false;
	}

	/** Checks text to see if part of it can be converted into inline styles. */
	private checkAddInlineStyle(node: Text, selection: Selection, iteration: Iteration): boolean {

		if (['B', 'I', 'CODE', 'A'].includes(node.parentElement!.tagName)) {
			return false;
		}

		// The match we'll use.
		let match: RegExpMatchArray | null = null;
		let tag: string;

		// Get the bold match.
		match = node.data.match(/(?<![\\*\w])(\*\*)(?! )[^*]+?(?<! )(\*\*)(?![*\w])/d);
		tag = 'b';
		// Get the italic match.
		if (!match) {
			match = node.data.match(/(?<![\\*\w])(\*)(?! )[^*]+?(?<! )(\*)(?![*\w])/d);
			tag = 'i';
		}
		// Get the code match.
		if (!match) {
			match = node.data.match(/(?<![\\\w])(`)(?! )[^`]+?(?<! )(`)(?!\w)/d);
			tag = 'code';
		}
		// Get the link match.
		if (!match) {
			match = node.data.match(/(?<![\\\w])(<)(?! )[^>]+?(?<! )(>)(?!\w)/d);
			tag = 'a';
		}
		if (!match) {
			return false;
		}

		// Split the text up and insert the new inline element.
		// console.log(`text -> ${tag}`);
		const firstSplitIndex = match.indices![1][0];
		const secondSplitIndex = match.indices![2][1];

		const nodeInsideStyle = this.splitText(node, firstSplitIndex, selection)!;
		this.splitText(nodeInsideStyle, secondSplitIndex - firstSplitIndex, selection);

		const styleNode = document.createElement(tag);
		if (tag === 'a') {
			(styleNode as HTMLAnchorElement).href = nodeInsideStyle.data.substring(1, nodeInsideStyle.data.length - 1);
		}
		node.after(styleNode);
		styleNode.append(nodeInsideStyle);
		iteration.setOffset(-1, -1);
		return true;
	}

	/** Check the heading, changing to a paragraph if necessary.
	 * Returns true if the heading was changed. */
	private checkHeading(node: HTMLHeadingElement, selection: Selection, iteration: Iteration): boolean {

		// If it no longer starts with the required #/#/# text, turn it into a paragraph.
		if (node.textContent === null || (node.tagName === 'H1' && !node.textContent.startsWith('# '))
			|| (node.tagName === 'H2' && !node.textContent.startsWith('## '))
			|| (node.tagName === 'H3' && !node.textContent.startsWith('### '))) {
			// console.log(`${node.tagName} -> p`);
			this.swapTag(node, 'p', selection);
			iteration.setOffset(-1, -1);
			return true;
		}

		return false;
	}

	/** Check if the li is valid, and if not convert it to a paragraph.
	 * Returns true if the conversion was done. */
	private checkLi(li: HTMLLIElement, selection: Selection, iteration: Iteration): boolean {

		// Don't check li's that are just parents of lists.
		if (li.firstChild instanceof HTMLUListElement || li.firstChild instanceof HTMLOListElement) {
			return false;
		}

		// Get the node level.
		let nodeLevel = 0;
		let nodeLevelNode = li.parentElement!.parentElement!;
		while (nodeLevelNode !== this.root) {
			nodeLevelNode = nodeLevelNode.parentElement!.parentElement!;
			nodeLevel += 1;
		}

		// Get the text level (the number of double spaces at the beginning).
		const textLevel = (li.textContent?.search(/[^ ]/) ?? NaN) / 2;

		// Check if it's even a valid list format.
		const validListFormat = li.textContent !== null && ((li.parentNode instanceof HTMLUListElement && li.textContent.match(/^( {2})*\* /) !== null)
			|| (li.parentNode instanceof HTMLOListElement && li.textContent.match(/^( {2})*[0-9]+\. /) !== null));

		// If the node level is not the text level or it's not even a valid list format, turn it into a paragraph.
		if (textLevel !== nodeLevel || !validListFormat) {

			// console.log('(ol/ul)+ li -> p');

			// Since we're changing multiple levels, we need to change the childOffsets of those levels.
			let childOffsetIndex = -1;

			// Go through each level and prepend, append, or split the lists as necessary to get to the top.
			do {
				// this.printNode(' --', this.root);

				// Get the parent node since we'll be using it a lot.
				const parent = li.parentElement!;
				const grandparent = parent.parentElement!;

				// Node was first child, so just insert it before the list.
				if (parent.firstChild === li) {

					// Add it before the list's parent li.
					if (grandparent instanceof HTMLLIElement) {
						grandparent.before(li);
						iteration.setOffset(childOffsetIndex - 2, -2);
						childOffsetIndex -= 2;

						// If the grandparent now contains an empty list, remove it.
						if (parent.childNodes.length === 0) {
							this.removeNodeWithSelection(grandparent, selection);
						}
					}
					// Add it before the list.
					else {
						parent.before(li);
						iteration.setOffset(childOffsetIndex - 2, -2);
						if (grandparent === this.root) {
							iteration.lastTopLevelIndex += 1;
						}

						// If the parent is now an empty list, remove it.
						if (parent.childNodes.length === 0) {
							this.removeNodeWithSelection(parent, selection);
							if (grandparent === this.root) {
								iteration.lastTopLevelIndex -= 1;
							}
						}
					}

					// If it's on the first iteration, renumber the ol list where the li was removed from.
					if (childOffsetIndex === -1 && parent instanceof HTMLOListElement) {
						this.renumberOl(parent);
					}
				}
				// Node was the last child, so just append it after the list.
				// No need to adjust child offsets, since they are after.
				else if (parent.lastChild === li) {

					// Go up one more level to add it after the list's parent li.
					if (grandparent instanceof HTMLLIElement) {
						grandparent.after(li);
						childOffsetIndex -= 2;
					}
					// Insert it after the list if the list is top level.
					else {
						parent.after(li);
						iteration.lastTopLevelIndex += 1;
					}
				}
				else { // We need to split the list in two.

					// Create the new list and add the children that follow the node to it.
					const newList = document.createElement(parent.tagName as 'ul' | 'ol');
					newList.append(...[...parent.childNodes].filter(
						(child) => child.compareDocumentPosition(li) === Node.DOCUMENT_POSITION_PRECEDING));

					// Insert the li node after the list if the list is top level.
					// No need to renumber the old list since only the end li's were removed.
					if (parent.parentElement === this.root) {
						parent.after(li);
						li.after(newList);
						iteration.lastTopLevelIndex += 2;
					}
					// or go up one more level to add it after the list's parent li.
					else {

						// Create an li since we're in a nested list.
						// No need to renumber, since the node is just passing through.
						parent.parentElement!.after(li);
						const li2 = document.createElement('li');
						li2.append(newList);
						li.after(li2);

						iteration.setOffset(childOffsetIndex - 2, -1);
						childOffsetIndex -= 2;
					}

					// If it's on the first iteration, renumber the ol list where the li was removed from.
					if (childOffsetIndex === -1 && parent instanceof HTMLOListElement) {
						this.renumberOl(parent);
					}
					// Also renumber the new list.
					if (newList instanceof HTMLOListElement) {
						this.renumberOl(newList);
					}
				}

				// this.printNode(' ++', this.root);
			} while (li.parentNode instanceof HTMLUListElement || li.parentNode instanceof HTMLOListElement);
			this.swapTag(li, 'p', selection);

			return true;
		}
		return false;
	}

	/** Change a p to a (ul/ol)+ li. */
	private checkParagraphToLi(node: HTMLParagraphElement, selection: Selection, iteration: Iteration): void {
		// this.printNode('before ', this.root);

		// Get which type of list it should be in.
		const ListType = node.textContent!.match(/^([ ][ ])*\* /) ? HTMLUListElement : HTMLOListElement;

		// Change the p to an li.
		const li = this.swapTag(node, 'li', selection);

		// Get the text level to know how many levels to move the new li in.
		const textLevel = li.textContent!.search(/[^ ]/) / 2;

		// For each two spaces, we'll move it further in.
		for (let i = 0; i <= textLevel; i++) {

			// Get the previous and next lists, if any. If we're not at level 0, we need to look at the sibling's child.
			const previous = i === 0 ? li.previousSibling : li.previousSibling?.lastChild;
			const next = i === 0 ? li.nextSibling : li.nextSibling?.firstChild;

			// this.printNode(`${i}/${textLevel} -- `, this.root);

			// If the previous is a list, put this new li at the end of that list.
			if ((i === textLevel && previous instanceof ListType)
				|| (i < textLevel && (previous instanceof HTMLUListElement || previous instanceof HTMLOListElement))) {
				previous.append(li);
				if (i === textLevel && previous instanceof HTMLOListElement) {
					this.renumberOl(previous);
				}

				// Join adjacent lists. It should only possibly join the next sibling.
				this.joinAdjacentLists(previous, selection, undefined);

				// Move back two nodes to recheck the previous list.
				if (i === 0) {
					iteration.setOffset(-1, -2);
					iteration.lastTopLevelIndex -= 1;
				}
			}
			// If there's no previous list, but the next is a list, put this new li at the beginning of that list.
			else if ((i === textLevel && next instanceof ListType)
				|| (i < textLevel && (next instanceof HTMLUListElement || next instanceof HTMLOListElement))) {
				next.prepend(li);
				if (i === textLevel && next instanceof HTMLOListElement) {
					this.renumberOl(next);
				}

				// Move back one node to recheck the next list.
				if (i === 0) {
					iteration.setOffset(-1, -1);
					iteration.lastTopLevelIndex -= 1;
				}
			}
			// If it's not adjacent to any list, create a new one.
			else {
				const newList = document.createElement(ListType === HTMLUListElement ? 'ul' : 'ol');
				if (i > 0) {
					const li2 = document.createElement('li');
					li.after(li2);
					li2.append(newList);
				}
				else {
					li.after(newList);
				}
				newList.append(li);
				if (i === textLevel && newList instanceof HTMLOListElement) {
					this.renumberOl(newList);
				}

				// Move back one node to recheck the new list.
				if (i === 0) {
					iteration.setOffset(-1, -1);
				}
			}

			// this.printNode(`${i}/${textLevel} ++ `, this.root);
		}
		// this.printNode('after ', this.root);
	}

	/** Checks if two uls or two ols are siblings, and if so joins them. */
	joinAdjacentLists(node: HTMLUListElement | HTMLOListElement, selection: Selection, iteration: Iteration | undefined): void {

		// Get the previous and next lists, if any. If we're not at level 0, we need to look at the sibling's child.
		const parent = node.parentElement!;
		const previous = parent instanceof HTMLLIElement ? parent.previousSibling?.lastChild : node.previousSibling;
		const next = parent instanceof HTMLLIElement ? parent.nextSibling?.firstChild : node.nextSibling;

		// Check the previous. If it's the same type as node, merge node into the previous.
		if ((node instanceof HTMLUListElement && previous instanceof HTMLUListElement)
			|| (node instanceof HTMLOListElement && previous instanceof HTMLOListElement)) {
			// console.log('Merging into previous', node);
			previous.append(...node.childNodes);
			this.removeNodeWithSelection(parent instanceof HTMLLIElement ? parent : node, selection);
			if (previous instanceof HTMLOListElement) {
				this.renumberOl(previous);
			}
			node = previous;

			// If childOffsets was supplied, adjust it so that the next child is the previousSibling.
			if (iteration) {
				if (parent instanceof HTMLLIElement) {
					iteration.setOffset(-2, -1);
				}
				else {
					iteration.setOffset(-1, -1);
					iteration.lastTopLevelIndex -= 1;
				}
			}
		}

		// Check the next. If it's the same type as node, merge node into the next.
		if ((node instanceof HTMLUListElement && next instanceof HTMLUListElement)
			|| (node instanceof HTMLOListElement && next instanceof HTMLOListElement)) {
			// console.log('Merging into next', node);
			next.prepend(...node.childNodes);
			this.removeNodeWithSelection(parent instanceof HTMLLIElement ? parent : node, selection);
			if (next instanceof HTMLOListElement) {
				this.renumberOl(next);
			}

			// If childOffsets was supplied, adjust it so that the next child is the nextSibling.
			if (iteration) {
				if (parent instanceof HTMLLIElement) {
					iteration.setOffset(-2, -1);
				}
				else {
					iteration.setOffset(-1, -1);
					iteration.lastTopLevelIndex -= 1;
				}
			}
		}
	}

	/** Renumber all of the li's in the ol. */
	renumberOl(ol: HTMLOListElement): void {

		// this.printNode('Renumbering:', ol);

		let numSpacesForOl = 0;
		let nodeForSpaces: Node = ol;
		while (nodeForSpaces.parentNode instanceof HTMLLIElement) {
			nodeForSpaces = nodeForSpaces.parentNode!.parentNode!;
			numSpacesForOl += 2;
		}

		// Go through each li, incrementing the number by one each time.
		let liNumber = 1;
		for (const li of ol.childNodes as NodeListOf<HTMLLIElement>) {

			// If li is actually just a holder for another list element, move on.
			if (li.firstChild instanceof HTMLOListElement || li.firstChild instanceof HTMLUListElement) {
				continue;
			}

			// If there's no number match, move on.
			if (li.textContent === null || li.textContent.match(/^( {2})*[0-9]+\. /) === null) {
				continue;
			}

			// Get how many spaces it has.
			let numSpaces = li.textContent.search(/[^ ]/);
			if (numSpaces === -1) {
				numSpaces = li.textContent.length;
			}

			// If it has the wrong number of spaces, move on.
			if (numSpaces !== numSpacesForOl) {
				continue;
			}
			const spaces = ''.padStart(numSpaces, ' ');

			// The number is already good, so move on.
			if (li.textContent.startsWith(`${spaces}${liNumber}`)) {
				liNumber += 1;
				continue;
			}

			// First remove all digits and spaces from the beginning of the li.
			// There may be a selection span in there, so we need to go over multiple nodes.
			for (let i = 0; i < li.childNodes.length; i++) {
				const child = li.childNodes[i];
				if (!(child instanceof Text)) {
					continue;
				}
				const match = child.data.match(/^[ 0-9]+/);
				if (!match) {
					break;
				}
				child.data = child.data.slice(match[0].length);
				if (child.data === '') {
					child.remove();
					i--;
				}
				else {
					break;
				}
			}

			// Now insert the number back in.
			if (!(li.firstChild instanceof Text)) {
				li.prepend(document.createTextNode(''));
			}
			(li.firstChild as Text).data = `${spaces}${liNumber}` + (li.firstChild as Text).data;

			// Increment the li number.
			liNumber += 1;
		}
	}

	/** Convert a p to a blockquote p. */
	private convertParagraphToBlockquote(node: HTMLParagraphElement, selection: Selection, iteration: Iteration): void {

		const previousSibling = node.previousSibling;
		const nextSibling = node.nextSibling;

		// If the previous sibling is a blockquote, append node into it.
		if (previousSibling instanceof HTMLQuoteElement) {
			// console.log('Merging p into previous blockquote sibling', node);
			previousSibling.append(node);
			iteration.setOffset(-1, -1);
			iteration.lastTopLevelIndex -= 1;

			// See if we can join the previousSibling and a nextSibling blockquote.
			if (nextSibling instanceof HTMLQuoteElement) {
				// console.log('Joining previous blockquote into the next blockquote', node);
				nextSibling.prepend(...previousSibling.childNodes);
				this.removeNodeWithSelection(previousSibling, selection);
				iteration.lastTopLevelIndex -= 1;
			}
		}

		// If the next sibling (among multiple) is a blockquote, merge node into it.
		else if (nextSibling instanceof HTMLQuoteElement) {
			// console.log('Merging p into next blockquote sibling', node);
			nextSibling.prepend(node);
			iteration.setOffset(-1, -1);
			iteration.lastTopLevelIndex -= 1;
		}

		// No siblings, so make a blockquote.
		else {
			// console.log('Making new blockquote from p.');
			const blockquote = document.createElement('blockquote');
			node.after(blockquote);
			blockquote.append(node);
		}
	}

	/** Checks if a blockquote paragraph is still valid. */
	private checkBlockquote(node: HTMLParagraphElement, selection: Selection, iteration: Iteration): boolean {

		// If it no longer starts with the required > text, move it outside of the blockquote.
		if (node.textContent !== null && node.textContent.startsWith('> ')) {
			return false;
		}

		// console.log(`p inside blockquote -> p outside of blockquote`);

		// Get the parent node since we'll be using it a lot.
		const parent = node.parentElement!;

		// Node was first child, so just insert it before the blockquote.
		if (parent.firstChild === node) {

			// Add it before the blockquotes's parent node.
			parent.before(node);
			iteration.setOffset(-2, -2);
			iteration.lastTopLevelIndex += 1;

			// If the blockquote is now empty, remove it.
			if (parent.childNodes.length === 0) {
				this.removeNodeWithSelection(parent, selection);
				iteration.lastTopLevelIndex -= 1;
			}
		}

		// Node was the last child of multiple children, so just append it after the blockquote.
		else if (parent.lastChild === node) {
			parent.after(node);
			iteration.lastTopLevelIndex += 1;
		}

		// Node was a middle child so we need to split the blockquote in two.
		else {

			// Create the new blockquote and add the children that follow the node to it.
			const newBlockquote = document.createElement('blockquote');
			newBlockquote.append(...[...parent.childNodes].filter(
				(child) => child.compareDocumentPosition(node) === Node.DOCUMENT_POSITION_PRECEDING));

			// Insert the node and new blockquote after the existing blockquote.
			parent.after(node, newBlockquote);

			// Adjust the iteration.
			if (parent === this.root) {
				iteration.lastTopLevelIndex += 2;
			}
		}

		return true;
	}

	/** Convert a p to a pre. */
	private convertParagraphToPre(node: HTMLParagraphElement, iteration: Iteration): void {

		// Search for any previous or next paragraph with ```. Only search paragraphs.
		let foundOtherNode = false;
		let startNode: HTMLParagraphElement = node;
		let endNode: HTMLParagraphElement = node;
		while (!foundOtherNode && startNode.previousSibling) {
			if (!(startNode.previousSibling instanceof HTMLParagraphElement)) {
				break;
			}
			startNode = startNode.previousSibling;
			if (startNode.textContent?.match(/^```$/)) {
				endNode = node;
				foundOtherNode = true;
			}
		}
		while (!foundOtherNode && endNode.nextSibling) {
			if (!(endNode.nextSibling instanceof HTMLParagraphElement)) {
				break;
			}
			endNode = endNode.nextSibling;
			if (endNode.textContent?.match(/^```$/)) {
				startNode = node;
				foundOtherNode = true;
			}
		}
		if (!foundOtherNode) {
			return;
		}

		// Create a pre and move all of the nodes into it.
		const pre = document.createElement('pre');
		const nodes: HTMLParagraphElement[] = [];
		for (let iterNode = startNode; iterNode !== endNode.nextSibling; iterNode = iterNode.nextSibling as HTMLParagraphElement) {
			nodes.push(iterNode);
		}
		endNode.after(pre);
		pre.append(...nodes);

		// Update the iteration.
		if (node === startNode) {
			iteration.setOffset(-1, -1);
		}
		else {
			iteration.setOffset(-1, -nodes.length);
		}
		iteration.lastTopLevelIndex -= nodes.length - 1;
	}

	/** Checks if a pre is still valid. If anything was changed, return true. */
	private checkPre(node: HTMLPreElement, selection: Selection, iteration: Iteration): boolean {

		// Make sure the first and last nodes are paragraphs the contain only ```.
		if (node.firstChild instanceof HTMLParagraphElement && node.firstChild.textContent === '```'
			&& node.lastChild instanceof HTMLParagraphElement && node.lastChild.textContent === '```') {
			return false;
		}

		// Remove the pre and move everything up.
		const numChildNodes = node.childNodes.length;
		node.after(...node.childNodes);
		this.removeNodeWithSelection(node, selection);

		// Update the iteration.
		iteration.setOffset(-1, -1);
		iteration.lastTopLevelIndex += numChildNodes - 1;

		return true;
	}

	/** Splits text, returning the node (if any) after the index. */
	splitText(node: Text, index: number, selection: Selection): Text | null {
		if (index > 0 && index < node.data.length) {
			const nextNode = node.splitText(index);
			if (selection?.startNode === node && selection.startOffset >= index) {
				selection.startNode = nextNode;
				selection.startOffset -= index;
			}
			if (selection?.endNode === node && selection.endOffset >= index) {
				selection.endNode = nextNode;
				selection.endOffset -= index;
			}
			return nextNode;
		}
		else if (index === 0) {
			return node;
		}
		else {
			return null;
		}
	}

	/** Swaps an element out with a new element with a different tag. Moves the children into the new node. Returns the new element. */
	swapTag(elem: Element, newTag: string, selection: Selection): Element {
		const newElem = document.createElement(newTag);
		elem.before(newElem);
		newElem.append(...elem.childNodes);
		if (selection?.startNode === elem) {
			selection.startNode = newElem;
		}
		if (selection?.endNode === elem) {
			selection.endNode = newElem;
		}
		this.removeNodeWithSelection(elem, selection);
		return newElem;
	}

	/** Removes a node, making sure the selection markers aren't removed but are moved to before and after the node. */
	removeNodeWithSelection(node: Node, selection: Selection): void {
		if (selection) {
			if (node.contains(selection.startNode)) {
				selection.startNode = node.parentNode!;
				selection.startOffset = this.getIndexOfNode(node);
			}
			if (node.contains(selection.endNode)) {
				selection.endNode = node.parentNode!;
				selection.endOffset = this.getIndexOfNode(node);
			}
		}
		node.parentNode!.removeChild(node);
	}

	/** Get index of node. */
	getIndexOfNode(node: Node): number {
		return Array.prototype.indexOf.call(node.parentNode!.childNodes, node);
	}

	/** Saves a selection by adding markers at the current selection anchor and focus positions. Returns the selection object. */
	private saveSelection(): Selection {

		// Get the selection. If there is none or both aren't Text nodes, just return nulls.
		const selection = window.getSelection();
		if (selection === null || selection.anchorNode === null || selection.focusNode === null) {
			return null;
		}

		// Make a start and end node/offset pair. Save whether the anchor is the start or not.
		let startNode = selection.anchorNode;
		let startOffset = selection.anchorOffset;
		let endNode = selection.focusNode;
		let endOffset = selection.focusOffset;
		let startIsAnchor = true;
		if (startNode.compareDocumentPosition(endNode) === Node.DOCUMENT_POSITION_PRECEDING) {
			[startNode, endNode] = [endNode, startNode];
			[startOffset, endOffset] = [endOffset, startOffset];
			startIsAnchor = false;
		}

		return {
			startNode,
			startOffset,
			endNode,
			endOffset,
			startIsAnchor
		};
	}

	/** Restores the selection to where the markers are. */
	restoreSelection(selectionToRestore: Selection): void {

		// If there was no selection, do nothing.
		if (!selectionToRestore) {
			return;
		}

		// Make sure the offsets are within range.
		if (selectionToRestore.startNode instanceof Text) {
			selectionToRestore.startOffset = Math.max(0, Math.min(selectionToRestore.startNode.data.length, selectionToRestore.startOffset));
		}
		else {
			selectionToRestore.startOffset = Math.max(0, Math.min(selectionToRestore.startNode.childNodes.length, selectionToRestore.startOffset));
		}
		if (selectionToRestore.endNode instanceof Text) {
			selectionToRestore.endOffset = Math.max(0, Math.min(selectionToRestore.endNode.data.length, selectionToRestore.endOffset));
		}
		else {
			selectionToRestore.endOffset = Math.max(0, Math.min(selectionToRestore.endNode.childNodes.length, selectionToRestore.endOffset));
		}

		// Set the new selection.
		const selection = window.getSelection();
		if (selection) {
			if (selectionToRestore.startIsAnchor) {
				selection.setBaseAndExtent(selectionToRestore.startNode, selectionToRestore.startOffset, selectionToRestore.endNode, selectionToRestore.endOffset);
			}
			else {
				selection.setBaseAndExtent(selectionToRestore.endNode, selectionToRestore.endOffset, selectionToRestore.startNode, selectionToRestore.startOffset);
			}
		}
	}

	/** The mutation observer. */
	private mutationObserver;

	/** A flag for when the first set of mutation observers happens (at initial load). */
	private firstMutation: boolean = true;

	/** The callback for when the text changes. Not triggered by setText calls. */
	private onChange?: () => void;

	protected static override html: string = `<div contentEditable="true"></div>`;

	protected static override css: string = `
		.MarkdownEditor {
			outline: none;
			white-space: pre-wrap;
		}
		.MarkdownEditor * {
			margin-top: 0;
			margin-bottom: 0;
		}
		.MarkdownEditor :is(p, h1, h2, h3, ul, ol, blockquote) + :is(p, h1, h2, h3, ul, ol, blockquote) {
			/* margin-top: 1rem; */
		}
		.MarkdownEditor :is(ol, ul) {
			padding-inline-start: .5rem;
			list-style: none;
		}
		.MarkdownEditor blockquote {
			padding-left: .5em;
		}
		.MarkdownEditor p.hr {
			border-bottom: 1px solid;
		}
		`;
}
