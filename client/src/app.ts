import { Component, ConnectionState, DropDownButton, FormCheckbox, FormEasy, FormInput, FormList, FormMultiSelect, FormRadioButton, FormTextArea, FormToggleButton, FullApp, Icon } from 'elm-app';

import { UserSettingsPage } from './pages/user-settings-page';
import { LoginPage } from './pages/login-page';
import { AdminPage } from './pages/admin-page';
import { ShareablePage } from './pages/shareable-page';

import html from './app.html';
import css from './app.css';

export class CedarDeskApp extends FullApp {

	/** Constructs the app. */
	constructor() {
		super();

		// Register all of the pages.
		this.registerPage('', ShareablePage);
		this.registerPage('admin', AdminPage);
		this.registerPage('user-settings', UserSettingsPage);
		this.registerPage('login', LoginPage);
		this.registerPage('shareables', ShareablePage);

		// Set the title.
		this.setTitleHtml('Cedar Desk');
	}

	protected onConnectionStateUpdated(prevState: ConnectionState, nextState: ConnectionState): void {
		const dropDownButton = this.queryComponent('.header > .menu.DropDownButton', DropDownButton);
		const dropDownContent = dropDownButton.getDropDownContent();
		if (prevState === 'authenticated') {
			this.queryComponent('.menu', DropDownButton).setClass('hidden', true);
			if (dropDownContent.queryHas('.admin')) {
				dropDownContent.removeNode(dropDownContent.query('.admin', Element));
			}
		}
		if (nextState === 'connecting') {
			this.pushPopup('<WaitingPopup text="Connecting..."></WaitingPopup>');
		}
		else if (nextState === 'connectingFailed') {
			this.popPopup();
			this.pushPopup('<WaitingPopup text="Could not connect to the server. Try waiting a bit and refreshing the page."></WaitingPopup>');
		}
		else if (nextState === 'authenticating') {
			this.popPopup();
			this.pushPopup('<WaitingPopup text="Authenticating..."></WaitingPopup>');
		}
		else if (nextState === 'authenticated') {
			this.popPopup();
			this.queryComponent('.menu', DropDownButton).setClass('hidden', false);
			void this.isAdmin().then((admin) => {
				if (admin && !dropDownContent.queryHas('.admin')) {
					const html = `<button class="button admin" onclick="goToPage|admin">Admin</button>`;
					dropDownContent.insertHtml(html, undefined, dropDownContent.query('.logout', Element), this);
				}
			});
		}
		else if (nextState === 'notAuthenticated') {
			this.popPopup();
			this.setRouterQuery({
				page: 'login',
				prevPage: this.getPageName() !== 'login' ? this.getPageName() : undefined
			}, { mergeOldQuery: true });
		}
	}

	/** Return where the page will go. */
	protected getPageParentAndBefore(): [Element, Element] {
		return [this.root, this.query('.toolbar', Element)];
	}

	/** Callback when a new page is shown. */
	protected onNewPage(): void {
		this.clearToolbarIcons();
	}

	/** Sets the title HTML. */
	setTitleHtml(html: string): void {
		this.query('.title', Element).innerHTML = html;
	}

	/** Sets the menu HTML. */
	protected setMenu(html: string): void {
		this.insertHtml(html, this.query('.menu', Element), undefined);
	}

	/** Adds a button to the toolbar. */
	addToolbarIcon(name: string, side: 'left' | 'right', iconUrl: string, alt: string, callback: (event: MouseEvent) => void): void {
		const toolbar = this.query('.toolbar', Element);
		if (this.queryHas(`.${name}`, toolbar)) {
			throw new Error(`There is already a toolbar icon with the name ${name}.`);
		}
		const html = `<button class="button ${name} icon ${side}"><icon src="${iconUrl}" alt="${alt}"></icon></button>`;
		const buttonNode = this.insertHtml(html, this.query(`:scope > .${side}`, Element, toolbar), undefined)[0] as HTMLButtonElement;
		buttonNode.addEventListener('click', callback);
	}

	/** Removes a button from the toolbar. */
	removeToolbarIcon(name: string): void {
		const toolbar = this.query('.toolbar', Element);
		try {
			const icon = this.query(`.${name}`, HTMLButtonElement, toolbar);
			this.removeNode(icon);
		}
		catch {
			throw new Error(`There is not a toolbar icon with the name ${name}.`);
		}
	}

	/** Gets a button element in the toolbar. */
	getToolbarIcon(name: string): HTMLButtonElement {
		const toolbar = this.query('.toolbar', Element);
		try {
			return this.query(`.${name}`, HTMLButtonElement, toolbar);
		}
		catch {
			throw new Error(`There is not a toolbar icon with the name ${name}.`);
		}
	}

	/** Gets if a button is in the toolbar. */
	hasToolbarIcon(name: string): boolean {
		const toolbar = this.query('.toolbar', Element);
		return this.queryHas(`.${name}`, toolbar);
	}

	/** Removes all buttons from the toolbar. */
	clearToolbarIcons(): void {
		this.clearNode(this.query('.toolbar .left', Element));
		this.clearNode(this.query('.toolbar .right', Element));
	}

	protected toggleMenu(): void {
		this.queryComponent('.menu', DropDownButton).setOpen('toggle');
	}

	/** Goes home. */
	protected goHome(): void {
		this.goToPage('');
	}

	/** Goes to the specified page. */
	protected override goToPage(page: string): void {

		// Close the menu.
		this.queryComponent('.menu', DropDownButton).setOpen(false);

		super.goToPage(page);
	}

	static override html = html;
	static override css = css;

	static override childComponentTypes: (typeof Component)[] = [
		DropDownButton,
		FormEasy,
		FormCheckbox,
		FormEasy,
		FormInput,
		FormList,
		FormMultiSelect,
		FormRadioButton,
		FormTextArea,
		FormToggleButton,
		Icon
	];
}

CedarDeskApp.setAppClass();
