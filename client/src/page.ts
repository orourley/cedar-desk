import { SimpleApp, Router } from 'elm-app';
import { CedarDeskApp } from './app';

export class Page extends SimpleApp.Page {

	/** Gets the CedarApp. */
	override get app(): CedarDeskApp {
		return super.app as CedarDeskApp;
	}

	override async initialize(): Promise<void> {
		await super.initialize();

		this.onPageScroll = this.onPageScroll.bind(this);
		this.root.addEventListener('scroll', this.onPageScroll);
	}

	/** Processes a router query. */
	override processQuery(_oldQuery: Router.Query | undefined, _query: Router.Query): Promise<void> {
		return Promise.resolve();
	}

	/** Called when the page scrolls. */
	private onPageScroll(): void {

		// // Clear any focused element to remove the mobile keyboard.
		// (document.activeElement as HTMLElement).blur();
	}
}
