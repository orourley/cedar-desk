import { Page } from '../page';
import { FormEasy, FormValues } from 'elm-app';
import { UserListing } from 'types';

import html from './admin-page.html';
import css from './admin-page.css';

export class AdminPage extends Page {
	override async initialize(): Promise<void> {
		// Set the title.
		this.app.setTitleHtml('Admin');

		// Populate the list of users.
		await this.populateUserList();
	}

	/** Gets the list of users. */
	private async populateUserList(): Promise<void> {

		// Get the list of users.
		const userListings = await this.app.send<Record<string, UserListing>>('users', 'listUsers', {});

		// Populate the HTML.
		const userListElem = this.query('.user-list', HTMLUListElement);
		userListElem.innerHTML = '';
		for (const [username, userListing] of Object.entries(userListings)) {
			this.insertHtml(`<li>${userListing.displayName} (${username})</li>`, userListElem, undefined);
		}
	}

	protected async createUser(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const username = values['username'] as string;
		const displayName = values['displayName'] as string;
		const password = values['password'] as string;
		const admin = values['admin'] as boolean;

		// Send the command.
		try {
			await this.app.send('users', 'createUser', {
				username,
				displayName,
				password,
				admin
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		// Update the list of users.
		await this.populateUserList();

		return 'The user has been created.';
	}

	protected async changePassword(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const username = values['username'] as string;
		const newPassword = values['newPassword'] as string;

		// Send the command.
		try {
			await this.app.send('users', 'changePassword', {
				username,
				newPassword
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		return 'The password has been changed.';
	}

	protected async changeAdmin(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const username = values['username'] as string;
		const admin = values['admin'] as boolean;

		// Send the command.
		try {
			await this.app.send('users', 'changeAdmin', {
				username,
				admin
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		return 'The admin status has been changed.';
	}

	protected async deleteUser(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const username = values['username'] as string;
		const verify = values['verify'] as string;

		if (verify !== 'DELETE') {
			return 'Please enter DELETE to confirm.';
		}

		// Send the command.
		try {
			await this.app.send('users', 'deleteUser', {
				username
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		// Update the list of users.
		await this.populateUserList();

		return 'The user has been deleted.';
	}

	static override html = html;
	static override css = css;
}
