// import { Page } from '../page';
// // import { ShowHide, FormEasy, Router, FormEasy, FormValues } from 'elm-app';
// // import { SunAlarm } from 'types';

// export class SunAlarmAddEditPage extends Page {
// 	override async initialize(): Promise<void> {

// 		// Set the title.
// 		this.app.setTitleHtml('Sun Alarm');
// 	}

// 	/** Processes the query. */
// 	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

// 		// Get the id from the router.
// 		this._alarmId = query['id'];
// 		if (this._alarmId !== undefined) {
// 			this.query('.title', Element).innerHTML = 'Edit Alarm';
// 			this.query('.submit', Element).innerHTML = 'Update Alarm';

// 			// this.element('form', HTMLElement).classList.add('hidden');
// 			// Fill in the inputs.
// 			const sunAlarm = await this.app.ws.send({
// 				module: 'sun-alarm',
// 				command: 'get',
// 				params: {
// 					id: this._alarmId
// 				}
// 			}) as SunAlarm;

// 			// Gather the form values from the class.
// 			const formValues: Map<string, string | boolean> = new Map();
// 			formValues.set('relativeTo', sunAlarm.relativeTo);
// 			formValues.set('angleOffset', `${Math.abs(sunAlarm.angleOffset)}`);
// 			formValues.set('angleSign', sunAlarm.angleOffset > 0 ? 'above' : 'below');
// 			formValues.set('timeOffset', `${Math.floor(Math.abs(sunAlarm.timeOffset) / 60)}`.padStart(2, '0') + ':' + `${Math.abs(sunAlarm.timeOffset) % 60}`.padStart(2, '0'));
// 			formValues.set('timeSign', sunAlarm.timeOffset > 0 ? 'after' : 'before');
// 			formValues.set('sound', sunAlarm.sound);
// 			for (let i = 0; i < 7; i++) {
// 				formValues.set(`day${i}`, sunAlarm.days[i]);
// 			}
// 			// Set the values of the form.
// 			this.queryComponent('.form', FormEasy).setValues(formValues);
// 			// Enable everything once we get the result.
// 			this.queryComponent('.form', FormEasy).setEnabled(true);
// 			// this.element('form', HTMLElement).classList.remove('hidden');
// 		}
// 		else {
// 			// Enable the form immediately.
// 			this.queryComponent('.form', FormEasy).setEnabled(true);
// 		}
// 	}

// 	protected _toggleAngleHelp(): void {
// 		ShowHide.toggle(this.root.querySelector('#angle-help') as HTMLElement);
// 	}

// 	protected async _submit(form: FormEasy, values: FormValues): Promise<string> {
// 		const inputs = form.getValues();
// 		console.log(inputs);

// 		// Get the angle offset.
// 		let angleOffset: number = 0;
// 		try {
// 			angleOffset = Number.parseFloat(values['angleOffset'] as string);
// 		}
// 		catch {
// 			return 'The angle offset must be a number.';
// 		}
// 		if (values['angleSign'] === 'below') {
// 			angleOffset *= -1;
// 		}

// 		// Get the time offset.
// 		let timeOffset: number = 0;
// 		try {
// 			const hours = Number.parseInt((values['timeOffset'] as string).substring(0, 2));
// 			const minutes = Number.parseInt((values['timeOffset'] as string).substring(3, 5));
// 			timeOffset = hours * 60 + minutes;
// 		}
// 		catch {
// 			return 'The time offset must in the format HH:MM.';
// 		}
// 		if (values['timeSign'] === 'before') {
// 			timeOffset *= -1;
// 		}

// 		// Get the days of the week.
// 		const days: boolean[] = [];
// 		for (let i = 0; i < 7; i++) {
// 			days.push(inputs[`day${i}`] as boolean);
// 		}

// 		// Add the alarm.
// 		try {
// 			await this.app.ws.send({
// 				module: 'sun-alarm',
// 				command: 'update',
// 				params: {
// 					id: this._alarmId,
// 					relativeTo: values['relativeTo'] as string,
// 					angleOffset: angleOffset,
// 					timeOffset: timeOffset,
// 					sound: values['sound'] as string,
// 					days: days,
// 					enabled: true
// 				}
// 			});
// 		}
// 		catch (error) {
// 			return (error as Error).message;
// 		}

// 		this._goToAlarmList();

// 		return '';
// 	}

// 	private _goToAlarmList(): void {
// 		this.app.setRouterQuery({
// 			page: 'sun-alarm'
// 		});
// 	}

// 	private _alarmId: string | undefined = undefined;

// 	protected html = /* html */`
// 		<div>
// 			<button onclick="_goToAlarmList" style="float: right">Back</button>
// 			<h1 class="title">Add Alarm</h1>
// 			<FormEasy class="form" onSubmitted="_submit" submitText="Save">
// 				<div class="entry">
// 					<p>Should the time of the alarm be relative to sunrise or sunset?</p>
// 					<FormRadioButton name="relativeTo" value="sunrise" checked>Sunrise</FormRadioButton>
// 					<FormRadioButton name="relativeTo" value="sunset">Sunset</FormRadioButton>
// 				</div>
// 				<div class="entry">
// 					<p>At what angle above or below the horizon (degrees)?</p>
// 					<FormInput name="angleOffset" value="0"></FormInput>
// 				</div>
// 				<div class="entry">
// 					<FormRadioButton name="angleSign" value="below" checked>Below</FormRadioButton>
// 					<FormRadioButton name="angleSign" value="above">Above</FormRadioButton>
// 					<button class="angle-help-button" ontoggle="_toggleAngleHelp">?</button>
// 					<div class="angle-help" class="popup2" style="display: none;">
// 						<p>When the sun is right at the horizon, it is at 0 degrees.</p>
// 						<p>Use negative numbers for angles below the horizon and positive numbers for angles above the horizon.</p>
// 						<p><img src="assets/images/angle-help.svg"></img></p>
// 						<p style="font-size: .5rem;">Courtesy Wikipedia</p>
// 					</div>
// 				</div>
// 				<div class="entry">
// 					<p>How much time before or after the sun reaches this angle (HH:MM)?</p>
// 					<FormInput name="timeOffset" value="00:00"></FormInput>
// 					<FormRadioButton name="timeSign" value="before" checked>before</FormRadioButton>
// 					<FormRadioButton name="timeSign" value="after">after</FormRadioButton>
// 				</div>
// 				<div class="entry">
// 					<p>Choose an alarm:</p>
// 					<FormRadioButton name="sound" value="beep" checked>Beep</FormRadioButton>
// 					<FormRadioButton name="sound" value="rooster">Rooster Crow</FormRadioButton>
// 					<FormRadioButton name="sound" value="lulluby">Lulluby</FormRadioButton>
// 				</div>
// 				<div class="entry">
// 					<p>On what days should it happen?</p>
// 					<FormCheckbox name="day0" checked>M</FormCheckbox>
// 					<FormCheckbox name="day1" checked>T</FormCheckbox>
// 					<FormCheckbox name="day2" checked>W</FormCheckbox>
// 					<FormCheckbox name="day3" checked>T</FormCheckbox>
// 					<FormCheckbox name="day4" checked>F</FormCheckbox>
// 					<FormCheckbox name="day5">S</FormCheckbox>
// 					<FormCheckbox name="day6">S</FormCheckbox>
// 				</div>
// 			</FormEasy>
// 		</div>
// 		`;

// 	protected css = /* css */`
// 		.SunAlarmAddEditPage {
// 			margin: 0 auto;
// 			width: 100%;
// 			max-width: 20rem;
// 		}
// 		.SunAlarmAddEditPage .days label {
// 			width: 1.5rem;
// 			text-align: center;
// 			padding: 0;
// 		}
// 	`;
// }
