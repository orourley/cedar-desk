import { Page } from '../page';
import { FormEasy, FormValues } from 'elm-app';
import { UserListing } from 'types';

import html from './user-settings-page.html';
import css from './user-settings-page.css';

export class UserSettingsPage extends Page {
	override async initialize(): Promise<void> {

		// Set the title.
		this.app.setTitleHtml('User Settings');

		// Get the display name for the user.
		const userListings = await this.app.send<Record<string, UserListing>>('users', 'listUsers', {});

		// Set the form value.
		this.queryComponent('.changeDisplayName > .FormEasy', FormEasy).setValues({
			displayName: userListings[this.app.username!].displayName
		});
	}

	protected async changeDisplayName(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const displayName = values['displayName'] as string;

		// Send the command.
		try {
			await this.app.send('users', 'changeDisplayName', {
				displayName
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		return 'Your display name has been updated.';
	}

	protected async changePassword(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const oldPassword = values['oldPassword'] as string;
		const newPassword = values['newPassword'] as string;
		const newPasswordAgain = values['newPasswordAgain'] as string;

		// Check if the new passwords match.
		if (newPassword !== newPasswordAgain) {
			return 'Your new password and retyped password do not match.';
		}

		// Send the command.
		try {
			await this.app.send('users', 'changePassword', {
				oldPassword,
				newPassword
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		return 'Your password has been changed.';
	}

	protected async deleteUser(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const password = values['password'] as string;
		const verify = values['verify'] as string;

		// Verify that DELETE was input.
		if (verify !== 'DELETE') {
			return 'Please enter DELETE to confirm.';
		}

		// Send the command.
		try {
			await this.app.send('users', 'deleteUser', {
				password
			});

			// Log the user out.
			await this.app.logout();
		}
		catch (error) {
			return (error as Error).message;
		}

		return '';
	}

	static override html = html;
	static override css = css;
}
