import { Page } from '../page';
import { FormEasy, FormValues, Router } from 'elm-app';

import html from './login-page.html';
import css from './login-page.css';

export class LoginPage extends Page {

	/** Processes the query. */
	override processQuery(oldQuery: Router.Query | undefined, _query: Router.Query): Promise<void> {

		// Save the old page so we can go back to it after login.
		this.prevPage = oldQuery?.['page'];
		if (this.prevPage === 'login') {
			this.prevPage = undefined;
		}

		return Promise.resolve();
	}

	/** Logs in. */
	protected async login(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the values.
		const username = values['username'] as string;
		const password = values['password'] as string;

		// Login.
		try {
			await this.app.login(username, password);
		}
		catch (error) {
			return (error as Error).message;
		}

		// Update the route.
		this.app.setRouterQuery({
			page: this.prevPage,
			prevPage: undefined
		}, { mergeOldQuery: true });

		return '';
	}

	private prevPage: string | undefined;

	static override html = html;
	static override css = css;
}
