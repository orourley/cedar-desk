import { JsonObject } from 'pine-lib';
import { NoteContent } from 'types';
import { Shareable } from './shareable';
import { MarkdownEditor } from '../../components/markdown-editor';

export class NoteEdit extends Shareable {

	override async initialize(content: JsonObject): Promise<void> {
		const noteContent = content as NoteContent;
		console.log(noteContent);

		this.queryComponent('.MarkdownEditor', MarkdownEditor).setText(noteContent.text);

		this.app.addToolbarIcon('view', 'right', 'assets/icons/eye.svg', 'View', this.goToMode.bind(this, 'view'));
		this.app.addToolbarIcon('save', 'right', 'assets/icons/save.svg', 'Save', this.saveContent.bind(this));
	}

	/** Sets the changed flag whenever any input changes. */
	protected inputChanged(): void {
		this.app.setConfirmOnRoute(true);
	}

	private async saveContent(): Promise<string> {

		// Push the saving popup.
		this.app.pushPopup('<WaitingPopup text="Saving..."></WaitingPopup>');

		const text = this.queryComponent('.MarkdownEditor', MarkdownEditor).getText();

		// Send the updated recipe.
		try {
			await this.app.send<string>('shareable', 'updateShareableContent', {
				path: this.path,
				content: {
					text
				}
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// All changes have been saved.
		this.app.setConfirmOnRoute(false);

		// Pop the saving popup.
		this.app.popPopup();

		return 'Saved';
	}

	protected static override html: string = `<div><MarkdownEditor onchange="inputChanged"></MarkdownEditor></div>`;

	protected static override css: string = `
		.NoteEdit {
			display: grid;
		}
		@media only print {
			.NoteEdit {
				display: block;
			}
		}
		`;

	static override childComponentTypes = [
		MarkdownEditor
	];
}
