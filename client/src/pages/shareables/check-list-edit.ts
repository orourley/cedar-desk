import { FormCheckbox, FormEasy, FormTextArea, FormValues, HierarchyDragList, RandomString, Sanitizer, UserContent, ConfirmPopup, CustomPopup } from 'elm-app';
import { JsonObject } from 'pine-lib';
import { CheckListContent } from 'types';
import { Shareable } from './shareable';

import html from './check-list-edit.html';
import css from './check-list-edit.css';

export class CheckListEdit extends Shareable {

	override async initialize(content: JsonObject): Promise<void> {
		const checkListContent = content as CheckListContent;

		// Set the flags.
		this.removeOnCheck = checkListContent.removeOnCheck;
		this.toggleDrag = checkListContent.toggleDrag;

		// Set the drag list.
		const html = /* html */`<HierarchyDragList onItemsMoved="onItemsMoved" onItemLevelChanged="onItemLevelChanged"${this.toggleDrag ? ' nograb' : ''}></HierarchyDragList>`;
		this.setHtml(html, this.root, this);
		this.hierarchyDragList = this.queryComponent('.HierarchyDragList', HierarchyDragList);
		for (const item of checkListContent.items) {
			this.insertItem(item.checked, item.id, item.level, item.text, undefined);
		}
		// Add one item if the check list is empty (as in a new checklist).
		if (checkListContent.items.length === 0) {
			const itemContent = this.insertItem(false, undefined, 0, '', undefined);
			await this.sendAddItemCommand(itemContent);
		}

		// Add a toolbar icons.
		if (this.toggleDrag) {
			this.app.addToolbarIcon('check-list-toggle-drag', 'right', 'assets/icons/grab.svg', 'Toggle Drag', this.toggleDragging.bind(this));
		}
		this.app.addToolbarIcon('check-list-settings', 'right', 'assets/icons/wrench.svg', 'Settings', this.openSettingsPopup.bind(this));
		this.app.addToolbarIcon('view', 'right', 'assets/icons/eye.svg', 'View', this.goToMode.bind(this, 'view'));

		// Set the update interval for changed items.
		this.intervalId = window.setInterval(() => {
			this.saveTextChangedElems();
		}, 1000);
	}

	/** The destructor. */
	override destroy(): void {
		// Save any of our pending text changes.
		this.saveTextChangedElems();
		window.clearInterval(this.intervalId);
		// Call the super.
		super.destroy();
	}

	override async handleCommand(command: string, params: JsonObject): Promise<void> {
		if (command === 'setField') {
			const field = params['field'] as string;
			const [_items, id, property] = field.split('.');
			const item = this.idsToItems.get(id);
			if (item === undefined) {
				return;
			}
			// Set the checked, text, or level of an item.
			if (property !== undefined) {
				const value = params['value'];
				if (property === 'checked' && typeof value === 'boolean') {
					// Check the item.
					const checkbox = this.getCheckbox(item);
					checkbox.value = value;
					item.root.classList.toggle('checked', value);
				}
				else if (property === 'text' && typeof value === 'string') {
					const textArea = this.getTextArea(item);
					textArea.value = value;
				}
				else if (property === 'level' && typeof value === 'number') {
					while (true) {
						const level = this.hierarchyDragList.getLevel(item);
						if (level === undefined || level === value) {
							break;
						}
						this.hierarchyDragList.shiftItem(item, value < level ? 'left' : 'right', false);
					}
				}
			}
			// Remove an item.
			else {
				const id = item.root.getAttribute('data-id');
				if (id === null) {
					return;
				}
				this.hierarchyDragList.removeItem(item);
				this.idsToItems.delete(id);
			}
		}
		// Insert a new item.
		else if (command === 'insertField') {
			const field = params['field'] as string;
			const [_items, beforeId] = field.split('.');
			const { id, checked, text, level } = params['value'] as { id: string, checked: boolean, text: string, level: number };
			this.insertItem(checked, id, level, text, beforeId !== '' ? beforeId : undefined);
		}
	}

	/** The hierarchy drag list items moved handler. */
	protected async onItemsMoved(items: UserContent[]): Promise<void> {
		for (const item of items.reverse()) {
			await this.sendRemoveItemCommand(item);
			await this.sendAddItemCommand(item);
		}
	}

	/** The hierarchy drag list item level changed handler. */
	protected onItemLevelChanged(item: UserContent): Promise<void> {
		return this.sendUpdateLevelCommand(item);
	}

	/** When one of the item's text values have changed. */
	protected onChanged(event: InputEvent): void {
		// Get the relative item and its input.
		const item = this.hierarchyDragList.getItemContaining(event.target as Element);
		if (item === undefined) {
			return;
		}
		const textArea = this.getTextArea(item);
		// Check for an enter pressed.
		// Needed here and not in keydown because Android keyboard doesn't send a proper enter event.
		if (textArea.value.includes('\n')) {
			const selection = textArea.getSelection();
			textArea.value = textArea.value.replace('\n', '');
			textArea.setSelection({ start: selection.start - 1, end: selection.start - 1 });
			void this.processEnterKey(item);
		}
		// Update the changed items list.
		this.textChangedItems.add(item);
	}

	/** When a key was pressed while focused on one of the items. */
	protected onKeyDown(event: KeyboardEvent): void {
		// Get the relative item and its input.
		const item = this.hierarchyDragList.getItemContaining(event.target as Element);
		if (item === undefined) {
			return;
		}
		const textArea = this.getTextArea(item);
		// Android keyboards don't report the correct keys for when enter is pressed, so check if there's an enter in the input.
		if (event.key === 'Enter' || textArea.value.includes('\n')) {
			void this.processEnterKey(item);
			// Prevent the actual enter from happening.
			event.preventDefault();
		}
		else if (event.key === 'Backspace' || event.key === 'Delete') {
			// If it is already blank,
			const selection = textArea.getSelection();
			if (event.key === 'Backspace'
					&& selection.start === 0 && selection.end === 0
					&& this.hierarchyDragList.getPrevItem(item) !== undefined) {
				this.processBackspaceKey(item);
				// Make the event not do an actual backspace.
				event.preventDefault();
			}
			else if (event.key === 'Delete'
					&& selection.start === textArea.value.length && selection.end === textArea.value.length
					&& this.hierarchyDragList.getNextItem(item) !== undefined) {
				this.processDeleteKey(item);
				// Make the event not do an actual backspace.
				event.preventDefault();
			}
		}
		else if (event.key === '[' && event.ctrlKey) {
			this.hierarchyDragList.shiftItem(item, 'left', true);
		}
		else if (event.key === ']' && event.ctrlKey) {
			this.hierarchyDragList.shiftItem(item, 'right', true);
		}
	}

	/** Process the enter key in the case where two items are split (this is always the case). */
	private async processEnterKey(item: UserContent): Promise<void> {
		const textArea = this.getTextArea(item);
		// Create new item below this one at the same level.
		let itemLevel = this.hierarchyDragList.getLevel(item)!;
		const itemChecked = this.getChecked(item);
		const nextItem = this.hierarchyDragList.getNextItem(item);
		const nextItemId = nextItem ? this.getId(nextItem) : undefined;
		const itemText = textArea.value.substring(textArea.getSelection().end);
		const itemChildren = this.getChildItems(item);
		if (textArea.getSelection().start !== 0 && itemChildren.length > 0) {
			itemLevel += 1;
		}
		this.insertItem(itemChecked, undefined, itemLevel, itemText, nextItemId);
		// Remove any text to the right of the cursor from the old input.
		textArea.value = textArea.value.substring(0, textArea.getSelection().start);
		// Move focus to new item.
		const newItem = this.hierarchyDragList.getNextItem(item)!;
		const newTextArea = this.getTextArea(newItem);
		newTextArea.focus();
		newTextArea.setSelection({ start: 0, end: 0 });
		// Save both items.
		await Promise.all([
			this.sendUpdateTextCommand(item),
			this.sendAddItemCommand(newItem)
		]);
	}

	/** Process the backspace key in the case where two items are joined. */
	private processBackspaceKey(item: UserContent): void {
		const textArea = this.getTextArea(item);
		// Append any text in the input to the prev item.
		const prevItem = this.hierarchyDragList.getPrevItem(item)!;
		const prevTextArea = this.getTextArea(prevItem);
		const prevTextAreaLength = prevTextArea.value.length;
		prevTextArea.value += textArea.value;
		void this.sendUpdateTextCommand(prevItem);
		// If the item is the first child of prevItem, shift all of its children -1.
		if (this.hierarchyDragList.getLevel(item) === this.hierarchyDragList.getLevel(prevItem)! + 1) {
			this.hierarchyDragList.shiftItem(item, 'left', true);
		}
		// Remove the item from the list.
		this.removeItem(item);
		void this.sendRemoveItemCommand(item);
		// Focus on the end of the previous item.
		prevTextArea.focus();
		prevTextArea.setSelection({ start: prevTextAreaLength, end: prevTextAreaLength });
	}

	/** Process the delete key in the case where two items are joined. */
	private processDeleteKey(item: UserContent): void {
		const textArea = this.getTextArea(item);
		// Prepend any text in the input to the next item.
		const nextItem = this.hierarchyDragList.getNextItem(item)!;
		const nextTextArea = this.getTextArea(nextItem);
		textArea.value += nextTextArea.value;
		// Update the next item.
		void this.sendUpdateTextCommand(item);
		// If the item is the first child of prevItem, shift all of its children -1.
		if (this.hierarchyDragList.getLevel(item)! + 1 === this.hierarchyDragList.getLevel(nextItem)) {
			this.hierarchyDragList.shiftItem(nextItem, 'left', true);
		}
		// Remove the item from the list.
		this.removeItem(nextItem);
		void this.sendRemoveItemCommand(nextItem);
	}

	/** Called when the remove button is pressed. */
	protected removeButtonPressed(eventOrId: InputEvent | string): void {
		// Get the item.
		let item: UserContent;
		if (typeof eventOrId === 'string') {
			item = this.idsToItems.get(eventOrId)!;
		}
		else {
			const itemOfEvent = this.hierarchyDragList.getItemContaining(eventOrId.target as Element);
			if (itemOfEvent === undefined) {
				return;
			}
			item = itemOfEvent;
		}
		// Get the list of items to remove.
		const itemsToRemove = [item];
		const childItems = this.getChildItems(item);
		for (const childItem of childItems) {
			itemsToRemove.push(childItem);
		}
		// If there are more than one, do a popup.
		if (typeof eventOrId !== 'string' && itemsToRemove.length > 1) {
			this.app.pushPopup(`<ConfirmPopup text="Are you sure you want to remove these?" onNo="popupNoButtonPressed" onYes="popupYesButtonPressed|${this.getId(item)}"></CustomPopup>
				`, this);
			return;
		}
		else {
			// Do the removing.
			for (const itemToRemove of itemsToRemove) {
				void this.sendRemoveItemCommand(itemToRemove);
				this.hierarchyDragList.removeItem(itemToRemove);
				this.idsToItems.delete(this.getId(itemToRemove));
			}
		}
	}

	/** Called when the 'no' button is pressed on the remove confirm popup.
	 * Just closes the popup. */
	protected popupNoButtonPressed(): void {
		this.app.popPopup();
	}

	/** Called when the 'yes' button is pressed on the remove confirm popup.
	 * Closes the popup and removes the item. */
	protected popupYesButtonPressed(id: string): void {
		this.app.popPopup();
		this.removeButtonPressed(id);
	}

	/** Inserts an item. */
	private insertItem(checked: boolean, id: string | undefined, level: number, text: string, beforeId: string | undefined): UserContent {
		if (id === undefined) {
			id = RandomString.generate(16);
		}
		const html = /* html */`
			<button class="button grab icon" tabindex="-1"><icon src="assets/icons/grab.svg" alt="grab"></icon></button>
			<FormCheckbox class="check" name="checked"${checked ? ' checked' : ''} onChanged="${this.removeOnCheck ? 'removeButtonPressed' : 'onCheckboxChanged'}"></FormCheckbox>
			<FormTextArea name="text" onChanged="onChanged" onKeyDown="onKeyDown">${Sanitizer.sanitizeForHtml(text)}</FormTextArea>
			<button class="button remove icon" onclick="removeButtonPressed"><icon src="assets/icons/close.svg" alt="remove item"></icon></button>
			`;
		const beforeItem = beforeId !== undefined ? this.idsToItems.get(beforeId) : undefined;
		const item = this.hierarchyDragList.insertItem(html, beforeItem, level, this);
		item.root.classList.toggle('checked', checked);
		item.root.setAttribute('data-id', id);
		this.idsToItems.set(id, item);
		return item;
	}

	/** Removes an item. Checks with user if there is more than one item. */
	private removeItem(item: UserContent): boolean {
		this.hierarchyDragList.removeItem(item);
		this.idsToItems.delete(this.getId(item));
		return true;
	}

	/** When one of the items is checked. */
	protected onCheckboxChanged(event: Event): void {
		const item = this.hierarchyDragList.getItemContaining(event.target as Element);
		if (item === undefined) {
			return;
		}
		this.checkItem(item, this.getChecked(item));
	}

	/** Checks an item, checking children and parents. */
	private checkItem(item: UserContent, checked: boolean): void {
		// Check the item.
		const checkbox = this.getCheckbox(item);
		checkbox.value = checked;
		item.root.classList.toggle('checked', checked);
		// Send to the server.
		void this.sendUpdateCheckedCommand(item);
		// Update all children.
		const childItems = this.getChildItems(item);
		for (const childItem of childItems) {
			const checkbox = this.getCheckbox(childItem);
			checkbox.value = checked;
			childItem.root.classList.toggle('checked', checked);
			// Send to the server.
			void this.sendUpdateCheckedCommand(childItem);
		}
		// Update the parents.
		this.updateParentChecks(item);
	}

	/** Updates all ancestor checks of item by checking every child. */
	private updateParentChecks(item: UserContent): void {
		const itemLevel = this.hierarchyDragList.getLevel(item)!;
		// Find the parent element.
		let parent = this.hierarchyDragList.getPrevItem(item);
		let parentLevel;
		while (true) {
			if (parent === undefined) {
				return;
			}
			parentLevel = this.hierarchyDragList.getLevel(parent)!;
			if (parentLevel < itemLevel) {
				break;
			}
			parent = this.hierarchyDragList.getPrevItem(parent);
		}
		// Go through each child of the found parent and see if they are all checked.
		let allChecked = true;
		const childItems = this.getChildItems(parent);
		for (const childItem of childItems) {
			// See if it's checked.
			if (!this.getChecked(childItem)) {
				allChecked = false;
			}
		}
		// Check or uncheck the parent as needed.
		const checkbox = this.getCheckbox(parent);
		checkbox.value = allChecked;
		parent.root.classList.toggle('checked', allChecked);
		// Send to the server.
		void this.sendUpdateCheckedCommand(parent);
		// Continue upward.
		this.updateParentChecks(parent);
	}

	/** Sends commands for every item that changed its text. */
	private saveTextChangedElems(): void {
		for (const item of this.textChangedItems) {
			if (this.hierarchyDragList.getItemContaining(item.root) !== undefined) {
				void this.sendUpdateTextCommand(item);
			}
		}
	}

	/** Sends an add item command. */
	private async sendAddItemCommand(item: UserContent): Promise<void> {
		try {
			const beforeItem = this.hierarchyDragList.getNextItem(item);
			await this.app.send<string>('shareable', 'insertField', {
				path: this.path,
				field: `items.${beforeItem ? this.getId(beforeItem) : ''}`,
				value: {
					id: this.getId(item),
					checked: this.getChecked(item),
					text: this.getText(item),
					level: this.hierarchyDragList.getLevel(item)!
				}
			});
		}
		catch (error) {
			console.log(error);
		}
	}

	/** Sends a remove item command to an item and all its children. */
	private async sendRemoveItemCommand(item: UserContent): Promise<void> {
		try {
			// Send the command for the item.
			await this.app.send('shareable', 'setField', {
				path: this.path,
				field: `items.${this.getId(item)}`,
				value: undefined
			});
		}
		catch (error) {
			console.log(error);
		}
	}

	/** Sends an updateChecked command. */
	private async sendUpdateCheckedCommand(item: UserContent): Promise<void> {
		// Send the command for the item.
		await this.app.send('shareable', 'setField', {
			path: this.path,
			field: `items.${this.getId(item)}.checked`,
			value: this.getChecked(item)
		});
	}

	/** Sends an updateText command. */
	private async sendUpdateTextCommand(item: UserContent): Promise<void> {
		// Send the command for the item.
		await this.app.send('shareable', 'setField', {
			path: this.path,
			field: `items.${this.getId(item)}.text`,
			value: this.getText(item)
		});
		// Remove it from the changed inputs, since it has just been saved.
		this.textChangedItems.delete(item);
	}

	/** Sends an updateLevel command. */
	private async sendUpdateLevelCommand(item: UserContent): Promise<void> {
		// Send the command for the item.
		await this.app.send('shareable', 'setField', {
			path: this.path,
			field: `items.${this.getId(item)}.level`,
			value: this.hierarchyDragList.getLevel(item)!
		});
	}

	/** Gets the id of an item. */
	private getId(item: UserContent): string {
		return item.root.getAttribute('data-id')!;
	}

	/** Gets the checkbox of an item. */
	private getCheckbox(item: UserContent): FormCheckbox {
		return item.queryComponent('.FormCheckbox', FormCheckbox);
	}

	/** Gets the checked value of an item. */
	private getChecked(item: UserContent): boolean {
		return this.getCheckbox(item).value;
	}

	/** Gets the text area of an item. */
	private getTextArea(item: UserContent): FormTextArea {
		return item.queryComponent('.FormTextArea', FormTextArea);
	}

	/** Gets the text of an item. */
	private getText(item: UserContent): string {
		return this.getTextArea(item).value;
	}

	/** Get all children of an item. */
	private getChildItems(item: UserContent): UserContent[] {
		const childItems = [];
		const level = this.hierarchyDragList.getLevel(item)!;
		let nextItem = this.hierarchyDragList.getNextItem(item);
		while (nextItem !== undefined) {
			// Get the level and check if it is a child.
			const nextLevel = this.hierarchyDragList.getLevel(nextItem)!;
			if (nextLevel <= level) {
				break;
			}
			childItems.push(nextItem);
			nextItem = this.hierarchyDragList.getNextItem(nextItem);
		}
		return childItems;
	}

	/** Opens the settings popup. */
	protected openSettingsPopup(): void {
		const popupHtml = `
			<CustomPopup>
				<h1>Settings</h1>
				<FormEasy onSubmitted="saveSettingsPopup" submitText="Save" onCanceled="cancelSettingsPopup">
					<section><label>
						<p><span class="label">Remove On Check</span> <FormCheckBox name="removeOnCheck"${this.removeOnCheck ? ' checked' : ''}></FormCheckBox></p>
					</label></section>
					<section><label>
						<p><span class="label">Toggle Button for Dragging</span> <FormCheckBox name="toggleDrag"${this.toggleDrag ? ' checked' : ''}></FormCheckBox></p>
					</label></section>
				</FormEasy>
			</CustomPopup>
			`;
		this.app.pushPopup(popupHtml, this);
	}

	/** Cancels the settings popup. */
	protected cancelSettingsPopup(): void {
		this.app.popPopup();
	}

	/** Saves the settings popup. */
	protected async saveSettingsPopup(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the values.
		const removeOnCheck = values['removeOnCheck'] as boolean;
		const toggleDrag = values['toggleDrag'] as boolean;

		// Send the command for the fields.
		await this.app.send('shareable', 'setField', {
			path: this.path,
			field: 'removeOnCheck',
			value: removeOnCheck
		});
		await this.app.send('shareable', 'setField', {
			path: this.path,
			field: 'toggleDrag',
			value: toggleDrag
		});

		// Remove the popup.
		this.app.popPopup();

		// Refresh the page.
		this.app.refresh();

		return '';
	}

	/** Toggles dragging. */
	protected toggleDragging(): void {
		this.hierarchyDragList.setGrabButtonVisible(!this.hierarchyDragList.getGrabButtonVisible());
	}

	/** The drag list. */
	private hierarchyDragList!: HierarchyDragList;

	/** A mapping of ids to items. */
	private idsToItems: Map<string, UserContent> = new Map();

	/** The interval id for updating changed items. */
	private intervalId!: number;

	/** The set of changed items. */
	private textChangedItems: Set<UserContent> = new Set();

	/** Flag if items should be removed when checked. */
	private removeOnCheck: boolean = false;

	/** Flag if always dragging or a toggle button. */
	private toggleDrag: boolean = false;

	protected static override html = html;
	protected static override css = css;

	static override childComponentTypes = [
		ConfirmPopup,
		CustomPopup,
		HierarchyDragList,
		FormEasy
	];
}
