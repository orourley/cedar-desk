import { JsonObject } from 'pine-lib';
import { CheckListContent } from 'types';
import { Shareable } from './shareable';

export class CheckListView extends Shareable {

	override async initialize(content: JsonObject): Promise<void> {
		const checkListContent = content as CheckListContent;

		let html = '<ol>';
		for (const item of checkListContent.items) {
			html += `<li style="padding-left: ${item.level * 1.5}rem"><span class="check">${item.checked ? '<icon src="assets/icons/check.svg" alt="check"></icon>' : ''}</span>${item.text}</li>`;
		}
		html += '</ol>';
		this.setHtml(html, this.root, this);

		// Get the permission.
		const permission = await this.app.send<'none' | 'view' | 'edit' | 'full'>('shareable', 'getPermission', {
			path: this.path,
		});

		// Add the toolbar icon.
		if (permission === 'edit' || permission === 'full') {
			this.app.addToolbarIcon('edit', 'right', 'assets/icons/pencil.svg', 'Edit', this.goToMode.bind(this, 'edit'));
		}
	}

	protected static override html = '<div></div>';

	protected static override css = `
		.CheckListView .check {
			display: inline-block;
			width: 1rem;
			height: 1rem;
			margin-right: .25rem;
			stroke: var(--color6);
		}
		.CheckListView ol {
			margin: 0;
			padding: 0;
		}
		.CheckListView li::before {
			content: none;
		}
		`;
}
