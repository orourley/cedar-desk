import { Base64, JsonObject } from 'pine-lib';
import { FormEasy, FormValues, FormList, RandomString, FormTextArea, FormInput } from 'elm-app';
import { Image, Ingredient, Note, RecipeContent, Step } from 'types';
import { Shareable } from './shareable';

import html from './recipe-edit.html';
import css from './recipe-edit.css';

export class RecipeEdit extends Shareable {
	override async initialize(content: JsonObject): Promise<void> {

		const recipeContent = content as RecipeContent;

		// Get the form.
		const formEasy = this.queryComponent('.FormEasy', FormEasy);

		// Set the description and servings.
		formEasy.setValues({
			description: recipeContent.description,
			makesAmount: (recipeContent.makesAmount ?? '') + '',
			makesUnit: recipeContent.makesUnit
		});

		// Tools
		const toolsFormList = formEasy.getEntries().queryComponent('.tools .FormList', FormList);
		for (const tool of recipeContent.tools) {
			const item = toolsFormList.add(undefined);
			const formTextArea = item.queryComponent('.FormTextArea', FormTextArea);
			formTextArea.value = tool;
		}

		// Ingredients
		const ingredientsFormList = formEasy.getEntries().queryComponent('.ingredients .FormList', FormList);
		for (const ingredient of recipeContent.ingredients) {
			const item = ingredientsFormList.add(undefined);
			if (ingredient.heading) {
				item.setHtml(`<FormInput name="ingredients[+].heading" value="${ingredient.text}" autocomplete="off"></FormInput>`, undefined, this);
			}
			else {
				item.queryComponent('.amount.FormInput', FormInput).value = ingredient.amount;
				item.queryComponent('.unit.FormInput', FormInput).value = ingredient.unit;
				item.queryComponent('.text.FormTextArea', FormTextArea).value = ingredient.text;
			}
		}

		// Steps
		const stepsFormList = formEasy.getEntries().queryComponent('.steps .FormList', FormList);
		for (const step of recipeContent.steps) {
			const item = stepsFormList.add(undefined);
			if (step.heading) {
				item.setHtml(`<FormInput name="steps[+].heading" value="${step.text}" autocomplete="off"></FormInput>`, undefined, this);
			}
			else {
				const formTextArea = item.queryComponent('.FormTextArea', FormTextArea);
				formTextArea.value = step.text;
			}
		}

		// Notes
		const notesFormList = formEasy.getEntries().queryComponent('.notes .FormList', FormList);
		for (const note of recipeContent.notes) {
			const item = notesFormList.add(undefined);
			if (note.heading) {
				item.setHtml(`<FormInput name="notes[+].heading" value="${note.text}" autocomplete="off"></FormInput>`, undefined, this);
			}
			else {
				const formTextArea = item.queryComponent('.FormTextArea', FormTextArea);
				formTextArea.value = note.text;
			}
		}

		// Images
		const imagesFormList = formEasy.getEntries().queryComponent('.images .FormList', FormList);
		for (const image of recipeContent.images) {
			const item = imagesFormList.add(undefined);
			item.setHtml(/* html */`
				<img>
				<input type="hidden" name="images[+].id" value="${image.id}">
				<input type="hidden" name="images[].data" value="${image.data}">
				<FormTextArea name="images[].caption" autocomplete="off"></FormTextArea>
				`, undefined, this);
			const formTextArea = item.queryComponent('.FormTextArea', FormTextArea);
			formTextArea.value = image.caption;
			const imageElem = item.query('img', HTMLImageElement);
			imageElem.src = `data:image/webp;base64,${image.data}`;
		}

		// Add the toolbar icon.
		this.app.addToolbarIcon('view', 'right', 'assets/icons/eye.svg', 'View', this.goToMode.bind(this, 'view'));
		this.app.addToolbarIcon('save', 'right', 'assets/icons/save.svg', 'Save', this.saveFromToolbar.bind(this));
	}

	/** makes an input a heading. */
	protected makeHeading(name: string, event: MouseEvent): void {
		const formEasy = this.queryComponent('.FormEasy', FormEasy);
		const formList = formEasy.getEntries().queryComponent(`.${name} .FormList`, FormList);
		const item = formList.getItemContaining(event.target as HTMLElement)!;
		item.setHtml(`<FormInput name="${name}[+].heading" autocomplete="off"></FormInput>`, undefined, this);
	}

	/** Sets the changed flag whenever any input changes. */
	protected inputChanged(): void {
		this.app.setConfirmOnRoute(true);
	}

	/** When the image input changes, put a preview pic in. */
	protected imageInputChanged(event: Event): void {
		const inputElem = event.target as HTMLInputElement;
		this.inputChanged();
		inputElem.classList.add('hidden');
		const readyElem = inputElem.nextElementSibling!;
		readyElem.classList.remove('hidden');
		const files = inputElem.files;
		if (files && files.length > 0) {
			const file = files.item(0)!;
			readyElem.innerHTML = `<img class="ready" src="${URL.createObjectURL(file)}">`;
		}
	}

	/** Saves from the toolbar. */
	protected async saveFromToolbar(): Promise<void> {

		// Get the form.
		const formEasy = this.queryComponent('.FormEasy', FormEasy);

		// Trigger its submit button.
		return formEasy.triggerSubmit();
	}

	protected async save(_form: FormEasy, values: FormValues): Promise<string> {

		// Push the saving popup.
		this.app.pushPopup('<WaitingPopup text="Saving..."></WaitingPopup>');

		// Description
		const description = values['description'] as string;

		// Makes Amount
		let makesAmount: number | undefined;
		const makesAmountValue = values['makesAmount'] as string;
		if (makesAmountValue !== '') {
			makesAmount = Number(makesAmountValue);
			if (isNaN(makesAmount) || makesAmount <= 0) {
				return 'Servings amount must be a positive number or blank.';
			}
		}

		// Makes Unit
		const makesUnit = values['makesUnit'] as string;

		// Tools
		const tools: string[] = (values['tools'] ?? []) as string[];

		// Ingredients
		const ingredients: Ingredient[] = [];
		const ingredientValues = (values['ingredients'] ?? []) as ({ amount: string, unit: string, text: string } | { heading: string })[];
		for (const ingredientValue of ingredientValues) {
			if ('heading' in ingredientValue) {
				ingredients.push({
					amount: '',
					unit: '',
					text: ingredientValue.heading,
					heading: true
				});
			}
			else {
				ingredients.push({
					amount: ingredientValue.amount,
					unit: ingredientValue.unit,
					text: ingredientValue.text,
					heading: false
				});
			}
		}

		// Steps
		const steps: Step[] = [];
		const stepEntries = (values['steps'] ?? []) as ({ text: string } | { heading: string })[];
		for (const stepEntry of stepEntries) {
			if ('heading' in stepEntry) {
				steps.push({
					text: stepEntry.heading,
					heading: true
				});
			}
			else {
				steps.push({
					text: stepEntry.text,
					heading: false
				});
			}
		}

		// Notes
		const notes: Note[] = [];
		const noteEntries = (values['notes'] ?? []) as ({ text: string } | { heading: string })[];
		for (const noteEntry of noteEntries) {
			if ('heading' in noteEntry) {
				notes.push({
					text: noteEntry.heading,
					heading: true
				});
			}
			else {
				notes.push({
					text: noteEntry.text,
					heading: false
				});
			}
		}

		// Images
		const images: Image[] = [];
		const imageEntries = (values['images'] ?? []) as (({ id: string } | { file: FileList | null }) & { data: string, caption: string })[];
		const promises: Promise<void>[] = [];
		for (const imageEntry of imageEntries) {

			// Image Files
			if ('file' in imageEntry) {
				if (imageEntry.file !== null && imageEntry.file.length === 1) {

					// Get the file and convert it to an array buffer.
					const file = imageEntry.file[0];
					promises.push(file.arrayBuffer().then(async (arrayBuffer) => {

						const base64 = Base64.toBase64(arrayBuffer);
						const webpBase64 = await this.app.send<string>('shareable', 'convertToResizedWebpBase64', {
							image: base64,
							maxSize: 1024
						});

						try {
							// Add the file's new id and caption to the images list.
							images.push({
								id: RandomString.generate(16),
								data: webpBase64,
								caption: imageEntry.caption,
							});
						}
						catch (message) {
							throw `Error while uploading ${file.name}: ${message}`;
						}
					}));
				}
			}
			// Existing images.
			else {
				images.push(imageEntry);
			}
		}

		// Wait until all image uploads are done.
		try {
			await Promise.all(promises);
		}
		catch (error) {
			return (error as Error).message;
		}

		// Send the updated recipe.
		try {
			await this.app.send<string>('shareable', 'updateShareableContent', {
				path: this.path,
				content: {
					description,
					makesAmount,
					makesUnit,
					tools,
					ingredients,
					steps,
					notes,
					images
				}
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// All changes have been saved.
		this.app.setConfirmOnRoute(false);

		// Pop the saving popup.
		this.app.popPopup();

		return 'Saved';
	}

	static override html = html;
	static override css = css;
}
