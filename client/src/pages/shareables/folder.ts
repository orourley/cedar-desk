import { JsonObject } from 'pine-lib';
import { Component, CustomPopup, FormCheckbox, FormEasy, FormInput, FormList, FormRadioButton, FormSelect, FormValues, PressAndHold, Router, Sanitizer } from 'elm-app';
import { FolderContent, Permission, Permissions, UserListing, newCheckListContent, newFolderContent, newNoteContent, newRecipeContent } from 'types';
import { Shareable } from './shareable';

import css from './folder.css';
import { FileChooser } from './file-chooser';

export class Folder extends Shareable {

	/** Initializes the shareable. */
	override async initialize(content: JsonObject, query: Router.Query): Promise<void> {

		// Bind this to functions.
		this.pressAndHoldStart = this.pressAndHoldStart.bind(this);
		this.onClickObject = this.onClickObject.bind(this);
		this.toggleSelectObject = this.toggleSelectObject.bind(this);

		// Save any public view password.
		this.publicViewPassword = query['publicViewPassword'];

		// Get the content as folder content.
		const folderContent = content as FolderContent;

		// Get the permission of the folder.
		this.permission = await this.app.send<Exclude<Permission, 'none'>>('shareable', 'getPermission', {
			path: this.path,
		});

		// Populate the html with the folders.
		let html = '';
		for (const folderItem of folderContent.list) {
			const disabled = false;
			const isTransferOrigPath = false;
			html += `
				<div class="entry${disabled ? ' disabled' : ''}${isTransferOrigPath ? ' highlighted' : ''}">
					<icon src="assets/icons/${folderItem.type}.svg" alt="${folderItem.type}"></icon>
					${folderItem.isLink ? '<icon class="link" src="assets/icons/link.svg" alt="link"></icon>' : ''}
					${folderItem.starred ? '<icon class="star" src="assets/icons/star.svg" alt="star"></icon>' : ''}
					<div class="name">${folderItem.name}</div>
				</div>`;
		}
		this.setHtml(html, this.root, this);

		// Add the toolbar buttons.
		this.app.addToolbarIcon('share', 'right', 'assets/icons/share.svg', 'Share', this.openSharePopup.bind(this));
		this.app.addToolbarIcon('publicView', 'right', 'assets/icons/public-view.svg', 'Public View', this.openPublicViewPopup.bind(this));
		this.app.addToolbarIcon('move', 'right', 'assets/icons/move.svg', 'Move', this.openMovePopup.bind(this));
		this.app.addToolbarIcon('createLink', 'right', 'assets/icons/link.svg', 'Create Link', this.openCreateLinkPopup.bind(this));
		this.app.addToolbarIcon('star', 'right', 'assets/icons/star.svg', 'Star', this.toggleStarred.bind(this));
		this.app.addToolbarIcon('trash', 'right', 'assets/icons/trash.svg', 'Trash', this.openTrashPopup.bind(this));
		this.app.addToolbarIcon('create', 'right', 'assets/icons/plus.svg', 'Create', this.openCreatePopup.bind(this));

		// Hide buttons as appropriate.
		this.updateToolbarIcons();

		// Register long touch edit mode for each object.
		for (const objectElem of this.queryAll('.entry', HTMLElement)) {
			PressAndHold.enable(objectElem, this.pressAndHoldStart, this.toggleSelectObject);
			objectElem.addEventListener('click', this.onClickObject as EventListener);
			objectElem.addEventListener('click', this.onClickObject as EventListener);
		}
	}

	/** When the user is pressing and holding an object. */
	protected pressAndHoldStart(): void {
		this.pressingAndHolding = true;
	}

	/** When an object is clicked. */
	protected onClickObject(event: Event): void {
		if (!this.pressingAndHolding) {
			return;
		}

		const elem = event.currentTarget as Element;
		if (this.selectedObjects.length === 0) {
			const name = Sanitizer.desanitizeFromHtml(this.query('.name', Element, elem).innerHTML);
			const path = `${this.path}/${name}`;
			this.app.setRouterQuery({
				path
			});
		}
		else {
			void this.toggleSelectObject(elem);
		}
		event.preventDefault();
	}

	/** Selects or deselects objects. */
	private async toggleSelectObject(elem: Element): Promise<void> {

		// The user is no longer pressing and holding.
		this.pressingAndHolding = false;

		// Get the object element and its selectedObjects index (could be -1).
		const selectedObjectsIndex = this.selectedObjects.findIndex((value) => value.elem === elem);

		// If we're selecting, just deselect and return.
		if (selectedObjectsIndex !== -1) {

			// Remove from the list.
			this.selectedObjects.splice(selectedObjectsIndex, 1);
			elem.classList.remove('highlighted');
		}
		else { // We're selecting an object.
			try {

				// Get the elem, name, and permission of the object we'll be editing.
				const name = Sanitizer.desanitizeFromHtml(this.query('.name', Element, elem).innerHTML);
				const permission = await this.app.send<'none' | 'view' | 'edit' | 'full'>('shareable', 'getPermission', {
					path: `${this.path}/${name}`
				});
				if (permission === 'none') {
					return;
				}

				// Add it to the list.
				this.selectedObjects.push({
					elem,
					name,
					permission
				});
				elem.classList.add('highlighted');
			}
			catch (error) {
				console.log(error);
				return;
			}
		}

		// Update the toolbar icons.
		this.updateToolbarIcons();
	}

	private updateToolbarIcons(): void {

		// Create.
		let create = false;
		if (this.selectedObjects.length === 0 && this.permission !== 'view') {
			create = true;
		}
		this.app.getToolbarIcon('create').classList.toggle('hidden', !create);

		// Create link.
		let createLink = false;
		if (this.selectedObjects.length <= 1) {
			createLink = true;
		}
		this.app.getToolbarIcon('createLink').classList.toggle('hidden', !createLink);

		// Star
		let star = false;
		if (this.selectedObjects.length > 0 && this.permission !== 'view') {
			star = true;
		}
		this.app.getToolbarIcon('star').classList.toggle('hidden', !star);

		// Move.
		this.app.getToolbarIcon('move').classList.toggle('hidden', false);

		// Trash.
		let trash = false;
		if (this.selectedObjects.length > 0) {
			trash = this.selectedObjects.map((value) => value.permission === 'full').reduce((prev, curr) => prev && curr);
		}
		this.app.getToolbarIcon('trash').classList.toggle('hidden', !trash);

		// Share and public view.
		let shareAndPublicView = false;
		if (this.selectedObjects.length === 0 && this.permission === 'full') {
			shareAndPublicView = true;
		}
		else if (this.selectedObjects.length === 1 && this.selectedObjects[0].permission === 'full') {
			shareAndPublicView = true;
		}
		this.app.getToolbarIcon('share').classList.toggle('hidden', !shareAndPublicView);
		this.app.getToolbarIcon('publicView').classList.toggle('hidden', !shareAndPublicView);
	}

	/** Toggles the starred flag on the selected object. */
	private async toggleStarred(): Promise<void> {

		for (const selectedObject of this.selectedObjects) {
			const starred = this.queryHas('.Icon.star', selectedObject.elem);

			// Send the command to the server.
			try {
				await this.app.send('shareable', 'starShareable', {
					folder: this.path,
					name: selectedObject.name,
					starred: !starred
				});
			}
			catch (error) { /* empty */ }
		}

		// Refresh if the destination folder was the current folder.
		this.app.refresh();
	}

	/** Opens the move popup. */
	private openMovePopup(): void {

		// Find if we can move all of the objects (not just copy).
		let canMoveAll = true;
		for (const selectedObject of this.selectedObjects) {
			canMoveAll = canMoveAll && selectedObject.permission === 'full';
		}

		// Get the path and name.
		let path = this.path;
		let name = this.path.substring(this.path.lastIndexOf('/') + 1);
		if (this.selectedObjects.length === 1) {
			path = `${this.path}/${this.selectedObjects[0].name}`;
			name = this.selectedObjects[0].name;
		}
		else if (this.selectedObjects.length > 1) {
			path = 'multiple';
			name = 'multiple';
		}

		// Open the popup.
		this.app.pushPopup(`
			<CustomPopup class="folder move">
				<h1>Move, Copy, and Rename</h1>
				<FormEasy onSubmitted="doMove" submitText="Do it!" onCanceled="cancelPopup">
					<p>The object to be moved: ${Sanitizer.sanitizeForHtml(path)}</p>
					<section${canMoveAll ? '' : ' class="disabled"'}><label>
						<p><span class="label">Keep the original object:</span> <FormCheckbox name="keepOrig"${canMoveAll ? '' : ' checked'}></FormCheckbox></p>
					</label></section>
					<p class="label">Choose where to move the object:</p>
					<section>
						<FileChooser name="newFolder" folder="${Sanitizer.sanitizeForAttribute(this.path)}" filterType="folder"${this.publicViewPassword !== undefined ? ` publicViewPassword="${Sanitizer.sanitizeForAttribute(this.publicViewPassword)}"` : ''}></FileChooser>
					</section>
					${this.selectedObjects.length <= 1 ? `<section><label>
							<p class="label">Choose a name for the new or moved object:</p>
							<p><FormInput name="newName" value="${Sanitizer.sanitizeForAttribute(name)}"></FormInput></p>
					</label></section>` : ''}
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Opens the create link popup. */
	private openCreateLinkPopup(): void {

		if (this.selectedObjects.length > 1) {
			return;
		}

		// Get the path and name.
		let path = this.path;
		let name = this.path.substring(this.path.lastIndexOf('/') + 1);
		if (this.selectedObjects.length === 1) {
			path = `${this.path}/${this.selectedObjects[0].name}`;
			name = this.selectedObjects[0].name;
		}

		// Open the popup.
		this.app.pushPopup(`
			<CustomPopup class="folder createLink">
				<h1>Create a Link</h1>
				<FormEasy onSubmitted="doCreateLink" submitText="Do it!" onCanceled="cancelPopup">
					<p>Link will go to the object: ${Sanitizer.sanitizeForHtml(path)}</p>
					<p class="label">Choose where to create the link:</p>
					<section>
						<FileChooser name="newFolder" folder="${Sanitizer.sanitizeForAttribute(this.path)}" filterType="folder"${this.publicViewPassword !== undefined ? ` publicViewPassword="${Sanitizer.sanitizeForAttribute(this.publicViewPassword)}"` : ''}></FileChooser>
					</section>
					<section><label>
						<p class="label">Choose a name for the link:</p>
						<p><FormInput name="newName" value="${Sanitizer.sanitizeForAttribute(name)}"></FormInput></p>
					</label></section>
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Opens the trash popup. */
	private openTrashPopup(): void {

		// Get the path.
		let path = this.path;
		if (this.selectedObjects.length === 1) {
			path = `${this.path}/${this.selectedObjects[0].name}`;
		}
		else if (this.selectedObjects.length > 1) {
			path = 'multiple';
		}

		// Open the popup.
		this.app.pushPopup(`
			<CustomPopup class="folder trash">
				<h1>Trash</h1>
				<FormEasy onSubmitted="doTrash" submitText="Trash it!" onCanceled="cancelPopup">
					<p>The object to be trashed: ${Sanitizer.sanitizeForHtml(path)}</p>
					<p>Are you sure you want to trash this?</p>
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Opens the share popup. */
	private async openSharePopup(): Promise<void> {

		// Do nothing if multiple selected.
		if (this.selectedObjects.length > 1) {
			return;
		}

		// Get the path.
		let path = this.path;
		if (this.selectedObjects.length === 1) {
			path = `${this.path}/${this.selectedObjects[0].name}`;
		}

		// Get the list of users.
		const userListings = await this.app.send<Record<string, UserListing>>('users', 'listUsers', {});

		// Populate the user options.
		let userOptionsHtml = '<option value="*">All Users</option>';
		for (const username of Object.keys(userListings)) {
			if (username === this.app.username) {
				continue;
			}
			const userListing = userListings[username];
			userOptionsHtml += `<option value="${Sanitizer.sanitizeForAttribute(username)}">${Sanitizer.sanitizeForHtml(userListing.displayName)}</option>`;
		}

		const popup = this.app.pushPopup<CustomPopup>(`
			<CustomPopup class="folder share">
				<h1>Share</h1>
				<FormEasy onSubmitted="doShare" submitText="Save" onCanceled="cancelPopup">
					<p>The object to be shared: ${Sanitizer.sanitizeForHtml(path)}</p>
					<p>Add or remove user permissions. If a user is not in this list, permissions in parent folders apply.</p>
					<p>Choose your access options <button class="button" onclick="openShareHelpPopup">?</button>:</p>
					<section><FormList nograb>
						<FormSelect name="entries[+].username" class="username">
							${userOptionsHtml}
						</FormSelect>
						<FormSelect name="entries[].permission" class="permission">
							<option value="none">Forbid Access</option>
							<option value="view">View Only</option>
							<option value="edit">Edit Content</option>
							<option value="full">Full Access</option>
						</FormSelect>
					</FormList></section>
					<p>Copy the URL to your clipboard to share with others: <button class="button" onclick="copySharePathToClipboard|${Sanitizer.sanitizeForCallbackAttribute(path)}">Copy</button></p>
				</FormEasy>
			</CustomPopup>
			`, this);

		// Get the permissions for populating the popup.
		const permissions = await this.app.send<Permissions>('shareable', 'getPermissions', {
			path
		});

		const formEasy = popup.getUserContent().queryComponent('.FormEasy', FormEasy);
		const formList = formEasy.getEntries().queryComponent('.FormList', FormList);
		for (const username of Object.keys(permissions.permissions)) {
			const permission = permissions.permissions[username];
			const item = formList.add();
			item.queryComponent('.FormSelect.username', FormSelect).value = username;
			item.queryComponent('.FormSelect.permission', FormSelect).value = permission;
		}
	}

	/** Opens up the share help popup. */
	protected openShareHelpPopup(): void {
		this.app.pushPopup(`
			<CustomPopup class="folder shareHelp">
				<h1>Help</h1>
				<FormEasy onSubmitted="cancelPopup" submitText="Close">
					<p>You can hit the + button to add a new entry, and the x button to remove any entry.</p>
					<p>You can choose any user, or choose All Users to specify every user in CedarDesk. Individual user settings override any "All Users" setting.</p>
					<p>The access options:</p>
					<ul>
						<li>Forbid Access - If you have shared a parent folder, this will override it for this object and forbid any access to this object for the specified users.</li>
						<li>View Only - The specified users can only view the object, but not edit it or change its sharing or move or trash it.</li>
						<li>Edit Content - The specified users can view and edit the object, but not change its sharing or move or trash it.</li>
						<li>Full Access - The specified users can view, edit, change sharing, move or trash the object.</li>
					</ul>
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Copies a path to the clipboard for sharing with others. */
	protected copySharePathToClipboard(path: string): void {
		void navigator.clipboard.writeText(`${location.origin}${location.pathname}#path=${Sanitizer.sanitizeForUrlQueryParam(path)}`);
	}

	/** Opens the public view popup. */
	private async openPublicViewPopup(): Promise<void> {

		// Do nothing if multiple selected.
		if (this.selectedObjects.length > 1) {
			return;
		}

		// Get the path.
		let path = this.path;
		if (this.selectedObjects.length === 1) {
			path = `${this.path}/${this.selectedObjects[0].name}`;
		}

		// Get the permissions for populating the popup.
		const permissions = await this.app.send<Permissions>('shareable', 'getPermissions', {
			path
		});

		// Push the popup.
		this.app.pushPopup(`
			<CustomPopup class="folder publicView">
				<h1>Public Viewing</h1>
				<FormEasy onSubmitted="doPublicViewPassword" submitText="Save Settings" onCanceled="cancelPopup">
					<p>The object to be publicly viewed: ${Sanitizer.sanitizeForHtml(path)}</p>
					<p>A public view password allows you to give a URL to others so that they can view the object without having to be registered or logged in as users.</p>
					<p>If you leave the password empty, the object will just inherit the public viewing password (if any) from its parent.</p>
					<section class="publicViewPassword"><label>
						<p class="label">Public View Password:</p>
						<p><FormInput name="publicViewPassword" onChanged="updatePublicViewPopup" value="${typeof permissions.publicViewPassword === 'string' ? permissions.publicViewPassword : ''}"></FormInput></p>
					</label></section>
					<p><label>Check here if you want to forbid public viewing completely and not inherit the public viewing password from the object's parents: <FormCheckbox name="forbid" onChanged="updatePublicViewPopup"${permissions.publicViewPassword === false ? ' checked' : ''}></FormCheckbox></label></p>
					<p class="publicViewUrl">Copy the URL to your clipboard to share with others: <button class="button" onclick="copyPublicViewPathToClipboard|${Sanitizer.sanitizeForCallbackAttribute(path)}">Copy</button></p>
				</FormEasy>
			</CustomPopup>
			`, this);

		// Do an initial update.
		this.updatePublicViewPopup();
	}

	/** Copies a path to the clipboard for sharing with others. */
	protected copyPublicViewPathToClipboard(path: string): void {
		const popup = this.app.getTopPopup() as CustomPopup;
		const formEasy = popup.getUserContent().queryComponent('.FormEasy', FormEasy);
		const entries = formEasy.getEntries();
		const publicViewPassword = entries.queryComponent('.publicViewPassword .FormInput', FormInput).value;
		void navigator.clipboard.writeText(`${location.origin}${location.pathname}#path=${Sanitizer.sanitizeForUrlQueryParam(path)}&publicViewPassword=${Sanitizer.sanitizeForUrlQueryParam(publicViewPassword)}`);
	}

	/** Updates the state of the public view popup when something changed. */
	protected updatePublicViewPopup(): void {
		const popup = this.app.getTopPopup() as CustomPopup;
		const formEasy = popup.getUserContent().queryComponent('.FormEasy', FormEasy);
		const entries = formEasy.getEntries();
		const publicViewPassword = entries.queryComponent('.publicViewPassword .FormInput', FormInput).value;
		// Disable the public view password entry if public viewing is forbidden.
		const forbidChecked = entries.queryComponent('[name="forbid"]', FormCheckbox).value;
		entries.query('.publicViewPassword', Element).classList.toggle('disabled', forbidChecked);
		entries.query('.publicViewUrl', Element).classList.toggle('disabled', forbidChecked || publicViewPassword === '');
	}

	/** Opens the create popup. */
	protected openCreatePopup(): void {
		this.app.pushPopup(`
			<CustomPopup class="folder create">
				<h1>Create</h1>
				<FormEasy onSubmitted="doCreate" submitText="Create" onCanceled="cancelPopup">
					<section><label>
						<p class="label">Choose a Type</p>
						<p><FormSelect class="types" name="type">
							<option value="folder">Folder</option>
							<option value="note">Note</option>
							<option value="check-list">Check List</option>
							<option value="recipe">Recipe</option>
						</FormSelect></p>
					</label></section>
					<section><label>
						<p class="label">Enter a Name</p>
						<p><FormInput name="name" class="input"></FormInput></p>
					</label></section>
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Move, copy, rename a shareable. */
	protected async doMove(_form: FormEasy, values: FormValues): Promise<string | void> {

		// Get the values.
		const newFolder = values['newFolder'] as string;
		const keepOrig = values['keepOrig'] as boolean;

		if (this.selectedObjects.length <= 1) {
			const newName = values['newName'] as string;
			const oldPath = this.selectedObjects.length === 1 ? `${this.path}/${this.selectedObjects[0].name}` : this.path;

			// Send the command to the server.
			try {
				await this.app.send('shareable', 'transferShareable', {
					path: oldPath,
					newFolder,
					newName,
					keepOrig
				});
			}
			catch (error) {
				return `${error}`;
			}
		}
		else {
			// Send the commands to the server.
			for (const selectedObject of this.selectedObjects) {
				try {
					await this.app.send('shareable', 'transferShareable', {
						path: `${this.path}/${selectedObject.name}`,
						newFolder,
						newName: selectedObject.name,
						keepOrig
					});
				}
				catch (error) {
					return `${error}`;
				}
			}
		}

		// Remove the popup.
		this.app.popPopup();

		// Refresh if the destination folder was the current folder.
		if (!keepOrig && this.selectedObjects.length === 0) {
			this.app.setRouterQuery({
				path: this.path.substring(0, this.path.lastIndexOf('/'))
			}, { overwriteHistory: true });
		}
		else if (this.path === newFolder || !keepOrig) {
			this.app.refresh();
		}
	}

	/** Creates a link. */
	protected async doCreateLink(_form: FormEasy, values: FormValues): Promise<string | void> {

		// Get the values.
		const oldPath = this.selectedObjects.length === 1 ? `${this.path}/${this.selectedObjects[0].name}` : this.path;
		const newFolder = values['newFolder'] as string;
		const newName = values['newName'] as string;

		// Send the commands to the server.
		try {

			// Get the original shareable type.
			const type = await this.app.send<string>('shareable', 'getType', {
				path: oldPath
			});

			// Create the shareable link.
			await this.app.send('shareable', 'createShareable', {
				path: newFolder,
				name: newName,
				type: 'link',
				content: {
					path: oldPath,
					type: type
				}
			});
		}
		catch (error) {
			return `${error}`;
		}

		// Remove the popup.
		this.app.popPopup();

		// Refresh if the destination folder was the current folder.
		if (this.path === newFolder) {
			this.app.refresh();
		}
	}

	/** Trashes a shareable. */
	protected async doTrash(): Promise<string | void> {

		// Send the commands to the server.
		for (const selectedObject of this.selectedObjects) {
			try {
				await this.app.send('shareable', 'deleteShareable', {
					path: `${this.path}/${selectedObject.name}`
				});
			}
			catch (error) {
				return `${error}`;
			}
		}

		// Remove the popup.
		this.app.popPopup();

		// Refresh if the destination folder was the current folder.
		this.app.refresh();
	}

	/** Creates a shareable. */
	protected async doCreate(_form: FormEasy, values: FormValues): Promise<string | void> {

		// Get the form values.
		const type = values['type'] as string;
		const name = values['name'] as string;

		// Send the command to the server.
		try {
			await this.app.send('shareable', 'createShareable', {
				path: this.path,
				name,
				type,
				content: newShareables[type]
			});
		}
		catch (error) {
			return `${error}`;
		}

		// Remove the popup.
		this.app.popPopup();

		// Go to the newly created shareable in edit mode.
		this.app.setRouterQuery({
			path: `${this.path}/${name}`,
			mode: 'edit'
		});
	}

	/** Sets the sharing for a shareable. */
	protected async doShare(_form: FormEasy, values: FormValues): Promise<string | void> {

		// Get the form values.
		const path = this.selectedObjects.length === 1 ? `${this.path}/${this.selectedObjects[0].name}` : this.path;

		// Construct the permissions from the values.
		const permissions: Record<string, Permission> = {};
		if (values['entries'] !== undefined) {
			for (const { username, permission } of values['entries'] as { username: string, permission: Permission }[]) {
				permissions[username] = permission;
			}
		}

		// Send the command to the server.
		try {
			await this.app.send('shareable', 'updatePermissions', {
				path,
				permissions
			});
		}
		catch (error) {
			return `${error}`;
		}

		// Remove the popup.
		this.app.popPopup();
	}

	/** Sets the public view password for a shareable. */
	protected async doPublicViewPassword(_form: FormEasy, values: FormValues): Promise<string | void> {

		// Get the form values.
		const path = this.selectedObjects.length === 1 ? `${this.path}/${this.selectedObjects[0].name}` : this.path;
		const publicViewForbidden = values['forbid'] as boolean;
		let publicViewPassword: string | false | undefined = values['publicViewPassword'] as string;
		if (publicViewForbidden) {
			publicViewPassword = false;
		}
		else if (publicViewPassword === '') {
			publicViewPassword = undefined;
		}

		// Send the command to the server.
		try {
			await this.app.send('shareable', 'updatePublicViewPassword', {
				path,
				publicViewPassword
			});
		}
		catch (error) {
			return `${error}`;
		}

		// Remove the popup.
		this.app.popPopup();
	}

	/** Cancels the popup. */
	protected cancelPopup(): void {
		this.app.popPopup();
	}

	/** The public view password of the folder. */
	private publicViewPassword: string | undefined;

	/** The permission of the folder. */
	private permission: Exclude<Permission, 'none'> = 'view';

	/** Whether or not the user is pressing and holding. */
	private pressingAndHolding: boolean = false;

	/** The currently selected objects. */
	private selectedObjects: {
		elem: Element,
		name: string,
		permission: Exclude<Permission, 'none'>
	}[] = [];

	static override html = `<div></div>`;
	static override css = css;

	static override childComponentTypes: (typeof Component)[] = [
		CustomPopup,
		FormCheckbox,
		FormInput,
		FormList,
		FormRadioButton,
		FormSelect,
		FileChooser
	];
}

const newShareables: Record<string, JsonObject> = {
	'folder': newFolderContent,
	'note': newNoteContent,
	'check-list': newCheckListContent,
	'recipe': newRecipeContent,
};
