import { JsonObject } from 'pine-lib';
import { NoteContent } from 'types';
import { Shareable } from './shareable';
import { MarkdownEditor } from '../../components/markdown-editor';

export class NoteView extends Shareable {

	override async initialize(content: JsonObject): Promise<void> {
		const noteContent = content as NoteContent;
		this.queryComponent('.MarkdownEditor', MarkdownEditor).setText(noteContent.text);

		// Get the permission.
		const permission = await this.app.send<'none' | 'view' | 'edit' | 'full'>('shareable', 'getPermission', {
			path: this.path,
		});

		// Add the toolbar icon.
		if (permission === 'edit' || permission === 'full') {
			this.app.addToolbarIcon('edit', 'right', 'assets/icons/pencil.svg', 'Edit', this.goToMode.bind(this, 'edit'));
		}
	}

	protected static override html: string = `<div><MarkdownEditor readonly></MarkdownEditor></div>`;

	static override childComponentTypes = [
		MarkdownEditor
	];
}
