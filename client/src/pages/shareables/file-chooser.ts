import { Component, ConfirmPopup, FullApp, Params, Sanitizer } from 'elm-app';
import { ShareableForUser, isFolder, isLink } from 'types';
import { CedarDeskApp } from '../../app';

export class FileChooser extends Component {

	constructor(params: Params) {
		super(params);

		// Get the input element.
		this.inputElem = this.query(':scope > input', HTMLInputElement);

		// Get the name.
		const name = params.attributes.get('name');
		if (name !== undefined) {
			this.inputElem.name = name;
		}

		// Get the starting folder.
		const folder = params.attributes.get('folder');
		if (folder === undefined) {
			throw new Error('Must supply a folder to FileChooser.');
		}
		this.folder = folder;

		// Get some other attributes.
		this.filterPrefix = params.attributes.get('filterprefix');
		this.filterType = params.attributes.get('filtertype');
		this.publicViewPassword = params.attributes.get('publicviewpassword');

		// Get the select event handler.
		this.onSelect = params.eventHandlers.get('onselect');

		// Set the input value, if needed.
		if (this.filterType === 'folder') {
			this.inputElem.value = this.folder;
		}

		void this.populateList();
	}

	/** Gets the selected path. */
	getSelected(): string | undefined {
		if (this.filterType === 'folder') {
			return this.folder;
		}
		else {
			return this.selected;
		}
	}

	/** Populates the file list. */
	private async populateList(): Promise<void> {

		// Update the displayed path.
		this.query('.currentFolder.file .name', Element).innerHTML = this.folder;

		// Get the shareable.
		let shareable;
		try {
			shareable = await (this.app as FullApp).send<ShareableForUser>('shareable', 'viewShareable', {
				path: this.folder,
				publicViewPassword: this.publicViewPassword
			});
		}
		catch (error) {
			this.setHtml('No permissions for folder.', this.query('.files', Element), this);
			return;
		}

		// If it's a link, go through to the link.
		if (isLink(shareable)) {
			this.folder = shareable.content.path;
			return this.populateList();
		}

		// If it's not a link and not a folder, error.
		if (!isFolder(shareable)) {
			this.setHtml('Invalid folder.', this.query('.files', Element), this);
			return;
		}

		// Populate the list.
		let html = '';
		for (const folderItem of shareable.content.list) {
			const pathParam = `${this.folder}/${folderItem.name}`;

			// Set the disabled flag.
			let disabled = false;
			if (this.filterType !== undefined && folderItem.type !== 'folder' && folderItem.type !== this.filterType) {
				disabled = true;
			}
			if (this.filterPrefix !== undefined && (pathParam + '/').startsWith(this.filterPrefix + '/')) {
				disabled = true;
			}

			// Set the onclick action.
			let onclick = '';
			if (!disabled) {
				if (folderItem.type === 'folder') {
					onclick = `goToFolder|${Sanitizer.sanitizeForCallbackAttribute(pathParam)}`;
				}
				else {
					onclick = `select|${Sanitizer.sanitizeForCallbackAttribute(pathParam)}`;
				}
			}
			else {
				onclick = '';
			}

			// Do the html.
			html += `
				<div class="file${disabled ? ' disabled' : ''}" onclick="${onclick}">
					<icon src="assets/icons/${folderItem.type}.svg" alt="${folderItem.type}"></icon>
					${folderItem.isLink ? '<icon class="link" src="assets/icons/link.svg" alt="link"></icon>' : ''}
					<div class="name">${folderItem.name}</div>
				</div>`;
		}
		if (html === '') {
			html = '<div class="empty">Empty</div>';
		}
		this.setHtml(html, this.query('.files', Element), this);
	}

	/** Goes back one folder. */
	goBack(): void {

		// Get the previous folder.
		const prevFolder = this.folderHistory.pop();
		if (prevFolder === undefined) {
			return;
		}

		// Set it.
		this.folder = prevFolder;

		// Set the input value.
		if (this.filterType === 'folder') {
			this.inputElem.value = this.folder;
		}

		// Refresh the list.
		void this.populateList();
	}

	/** Goes up one folder. */
	goUp(): void {

		// Save the old folder.
		const oldFolder = this.folder;

		// Set the new folder to one higher.
		const folders = this.folder.split('/');
		if (folders.length > 1) {
			folders.pop();
		}
		this.folder = folders.join('/');

		// Set the input value.
		if (this.filterType === 'folder') {
			this.inputElem.value = this.folder;
		}

		// Push to the folder history.
		if (oldFolder !== this.folder) {
			this.folderHistory.push(oldFolder);
		}

		// Refresh the list.
		void this.populateList();
	}

	/** Goes to the home (user) folder. */
	goHome(): void {

		// Save the old folder.
		const oldFolder = this.folder;

		// Set the folder to home.
		const username = (this.app as CedarDeskApp).username;
		if (username === undefined) {
			return;
		}
		this.folder = username;

		// Set the input value.
		if (this.filterType === 'folder') {
			this.inputElem.value = this.folder;
		}

		// Push to the folder history.
		if (oldFolder !== this.folder) {
			this.folderHistory.push(oldFolder);
		}

		// Refresh the list.
		void this.populateList();
	}

	/** Goes to the specified folder. */
	goToFolder(folder: string): void {

		// Push the old folder to the history.
		if (this.folder !== folder) {
			this.folderHistory.push(this.folder);
		}

		// Set the folder.
		this.folder = folder;

		// Set the input value.
		if (this.filterType === 'folder') {
			this.inputElem.value = this.folder;
		}

		// Refresh the list.
		void this.populateList();
	}

	/** Selects a file. */
	select(file: string): void {
		this.inputElem.value = file;
		this.selected = file;
		if (this.onSelect) {
			this.onSelect(this.selected);
		}
	}

	/** Opens the help popup. */
	protected openHelpPopup(): void {
		(this.app as CedarDeskApp).pushPopup(`
			<CustomPopup class="fileChooser help">
				<h1>Help</h1>
				<FormEasy onSubmitted="closeHelpPopup" submitText="Close">
					<p>You can select a ${this.filterType} here.</p>
					<p>The left arrow button goes to the folder you were just at, similar to the back button on your browser.</p>
					<p>The up arrow button goes to the parent folder. If you are in a folder not owned by you, the parent folder may not be accessible.</p>
					<p>The home button goes to your home folder.</p>
				</FormEasy>
			</CustomPopup>
			`, this);
	}

	/** Closes the help popup. */
	protected closeHelpPopup(): void {
		(this.app as CedarDeskApp).popPopup();
	}

	/** The hidden input element. */
	private inputElem: HTMLInputElement;

	private folder: string = '';
	private filterPrefix: string | undefined;
	private filterType: string | undefined;
	private publicViewPassword: string | undefined;

	private folderHistory: string[] = [];

	private selected: string | undefined;
	private onSelect: ((...args: any[]) => any) | undefined;

	static override html = `
		<div>
			<input type="hidden">
			<div class="entry buttons">
				<button onclick="goBack" class="button icon"><icon src="assets/icons/arrow-left.svg" alt="back"></icon></button>
				<button onclick="goUp" class="button icon"><icon src="assets/icons/arrow-up.svg" alt="up"></icon></button>
				<button onclick="goHome" class="button icon"><icon src="assets/icons/home.svg" alt="home"></icon></button>
				<button onclick="openHelpPopup" class="button">?</button>
			</div>
			<div class="entry currentFolder file">
				<icon src="assets/icons/folder-open.svg" alt="Current Folder"></icon>
				<div class="name"></div>
			</div>
			<div class="entry files"></div>
		</div>
		`;

	static override css = `
		.FileChooser {
			border: 1px solid var(--color6);
			border-radius: .5rem;
			padding: .25rem;
			display: flex;
			flex-direction: column;
			gap: .25rem;
			overflow: auto;
		}
		.FileChooser .buttons {
			display: flex;
			gap: .25rem;
		}
		.FileChooser .file {
			position: relative;
			cursor: pointer;
			display: flex;
		}
		.FileChooser .file.disabled {
			cursor: not-allowed;
			opacity: 50%;
		}
		.FileChooser .file .Icon {
			display: inline-block;
			width: 1.25rem;
			height: 1.25rem;
			margin-right: .25rem;
		}
		.FileChooser .file .Icon.link {
			position: absolute;
			top: .55rem;
			left: .75rem;
			width: .75rem;
			height: .75rem;
		}
		.FileChooser .file .name {
			display: inline-block;
		}
		.FileChooser .files {
			overflow: auto;
			padding-left: 1rem;
			display: flex;
			flex-direction: column;
			gap: .25rem;
		}
		.FileChooser .files .empty {
			font-style: italic;
		}
		.Popup.fileChooser.help .window {
			width: 30rem;
		}
		`;

	static override childComponentTypes = [
		ConfirmPopup
	];
}
