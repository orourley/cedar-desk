import { JsonObject } from 'pine-lib';
import { LinkContent } from 'types';
import { Shareable } from './shareable';

export class Link extends Shareable {

	override async initialize(content: JsonObject): Promise<void> {
		const linkContent = content as LinkContent;

		// Just forward to the path link.
		this.app.setRouterQuery({
			path: linkContent.path
		}, { overwriteHistory: true });
	}

	protected static override html: string = '<div></div>';
}
