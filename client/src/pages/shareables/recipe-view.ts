import { JsonObject } from 'pine-lib';
import { ConfirmPopup, Sanitizer } from 'elm-app';
import { RecipeContent } from 'types';
import { Shareable } from './shareable';

import html from './recipe-view.html';
import css from './recipe-view.css';

export class RecipeView extends Shareable {
	override async initialize(content: JsonObject): Promise<void> {
		const recipeContent = content as RecipeContent;

		// Bind this to functions.
		this.closeImagePopup = this.closeImagePopup.bind(this);

		// Popuplate the description and makes part.
		this.query('.description', Element).innerHTML = recipeContent.description;
		this.query('.makesAmount', Element).innerHTML = recipeContent.makesAmount + '';
		this.query('.makesUnit', Element).innerHTML = recipeContent.makesUnit;
		if (recipeContent.makesAmount !== undefined || recipeContent.makesUnit !== '') {
			if (recipeContent.makesAmount === undefined) {
				this.query('.makesAmount', Element).remove();
			}
			if (recipeContent.makesUnit === '') {
				this.query('.makesUnit', Element).remove();
			}
		}
		else {
			this.query('.makes', Element).remove();
		}

		// Populate the tools.
		const toolsElem = this.query('.tools', Element);
		if (recipeContent.tools.length > 0) {
			let html = '<ul>';
			for (const tool of recipeContent.tools) {
				html += `<li>${tool}</li>`;
			}
			html += '</ul>';
			this.insertHtml(html, toolsElem.parentElement!, toolsElem.nextElementSibling ?? undefined, this);
		}
		else {
			this.removeNode(toolsElem);
		}

		// Populate the ingredients.
		const ingredientsElem = this.query('.ingredients', Element);
		if (recipeContent.ingredients.length > 0) {
			let html = '<ul>';
			for (const ingredient of recipeContent.ingredients) {
				if (ingredient.heading) {
					html += `</ul><h2>${ingredient.text}</h2><ul>`;
				}
				else {
					html += '<li>';
					if (ingredient.amount.length > 0) {
						html += `${this.makeNiceAmount(ingredient.amount)} `;
					}
					if (ingredient.unit.length > 0) {
						html += `${ingredient.unit} `;
					}
					html += ingredient.text;
					html += '</li>';
				}
			}
			html += '</ul>';
			this.insertHtml(html, ingredientsElem.parentElement!, ingredientsElem.nextElementSibling ?? undefined, this);
		}
		else {
			this.removeNode(ingredientsElem);
		}

		// Populate the steps.
		const stepsElem = this.query('.steps', Element);
		if (recipeContent.steps.length > 0) {
			let html = '<ol>';
			for (const step of recipeContent.steps) {
				if (step.heading) {
					html += `</ol><h2>${step.text}</h2><ol>`;
				}
				else {
					html += `<li>${step.text}</li>`;
				}
			}
			html += '</ol>';
			this.insertHtml(html, stepsElem.parentElement!, stepsElem.nextElementSibling ?? undefined, this);
		}
		else {
			this.removeNode(stepsElem);
		}

		// Populate the notes.
		const notesElem = this.query('.notes', Element);
		if (recipeContent.notes.length > 0) {
			let html = '<ul>';
			for (const step of recipeContent.notes) {
				if (step.heading) {
					html += `</ul><h2>${step.text}</h2><ul>`;
				}
				else {
					html += `<li>${step.text}</li>`;
				}
			}
			html += '</ul>';
			this.insertHtml(html, notesElem.parentElement!, notesElem.nextElementSibling ?? undefined, this);
		}
		else {
			this.removeNode(notesElem);
		}

		// Populate the images.
		const imagesElem = this.query('.images', Element);
		if (recipeContent.images.length > 0) {
			for (const image of recipeContent.images) {
				this.insertHtml(/* html */`
					<div class="image" onclick="openImagePopup|${Sanitizer.sanitizeForCallbackAttribute(image.caption)}|${image.data}">
						<img src="data:image/webp;base64,${image.data}">
						<div class="caption">${Sanitizer.sanitizeForHtml(image.caption)}</div>
					</div>`, imagesElem.parentElement!, undefined, this)[0].childNodes[0] as HTMLImageElement;
			}
		}
		else {
			this.removeNode(imagesElem);
		}

		// Get the permission.
		const permission = await this.app.send<'none' | 'view' | 'edit' | 'full'>('shareable', 'getPermission', {
			path: this.path,
		});

		// Add the toolbar icon.
		if (permission === 'edit' || permission === 'full') {
			this.app.addToolbarIcon('edit', 'right', 'assets/icons/pencil.svg', 'Edit', this.goToMode.bind(this, 'edit'));
		}
	}

	/** Make a string amount nicer. */
	private makeNiceAmount(amount: string): string {
		const tokens = amount.match(/^(\d* )?(\d+) ?\/ ?(\d+)$/);
		if (tokens === null) {
			return amount;
		}
		const wholeNumber = tokens[1] !== undefined ? tokens[1] + ' ' : '';
		const numerator = tokens[2];
		const denominator = tokens[3];
		if (numerator === '1') {
			if (denominator === '2') {
				return `${wholeNumber}½`;
			}
			else if (denominator === '3') {
				return `${wholeNumber}⅓`;
			}
			else if (denominator === '4') {
				return `${wholeNumber}¼`;
			}
			else if (denominator === '5') {
				return `${wholeNumber}⅕`;
			}
			else if (denominator === '6') {
				return `${wholeNumber}⅙`;
			}
			else if (denominator === '7') {
				return `${wholeNumber}⅐`;
			}
			else if (denominator === '8') {
				return `${wholeNumber}⅛`;
			}
			else if (denominator === '9') {
				return `${wholeNumber}⅑`;
			}
			else if (denominator === '10') {
				return `${wholeNumber}⅒`;
			}
		}
		else if (numerator === '2') {
			if (denominator === '3') {
				return `${wholeNumber}⅔`;
			}
			else if (denominator === '5') {
				return `${wholeNumber}⅖`;
			}
		}
		else if (numerator === '3') {
			if (denominator === '4') {
				return `${wholeNumber}¾`;
			}
			else if (denominator === '5') {
				return `${wholeNumber}⅗`;
			}
			else if (denominator === '8') {
				return `${wholeNumber}⅜`;
			}
		}
		else if (numerator === '4') {
			if (denominator === '5') {
				return `${wholeNumber}⅘`;
			}
		}
		else if (numerator === '5') {
			if (denominator === '6') {
				return `${wholeNumber}⅚`;
			}
			else if (denominator === '8') {
				return `${wholeNumber}⅝`;
			}
		}
		else if (numerator === '7') {
			if (denominator === '8') {
				return `${wholeNumber}⅞`;
			}
		}
		return amount;
	}

	/** Open the image popup. */
	openImagePopup(caption: string, data: string, event: Event): void {
		this.app.pushPopup(`
			<CustomPopup class="recipeViewImage">
				<div>
					<img src="data:image/webp;base64,${data}"></img>
					<div class="caption">${Sanitizer.sanitizeForHtml(caption)}</div>
				</div>
			</CustomPopup>
			`, this);
		this.app.addEventListener('click', this.closeImagePopup);
		event.stopPropagation();
	}

	/** Close the image popup. */
	closeImagePopup(): void {
		this.app.removeEventListener('click', this.closeImagePopup);
		this.app.popPopup();
	}

	static override html = html;
	static override css = css;

	static override childComponentTypes = [
		ConfirmPopup
	];
}
