import { JsonObject } from 'pine-lib';
import { Params, Router } from 'elm-app';
import { CedarDeskComponent } from '../../cedar-desk-component';

export class Shareable extends CedarDeskComponent {

	setPathAndMode(path: string, mode: 'view' | 'edit'): void {
		this._path = path;
		this._mode = mode;
	}

	/** Initialize the shareable. */
	async initialize(_content: JsonObject, _query: Router.Query): Promise<void> {}

	/** Handles a command from the server. */
	async handleCommand(_command: string, _params: JsonObject): Promise<void> {}

	/** Constructs this. */
	constructor(params: Params) {
		super(params);

		// Bind functions to this.
		this.handleCommand = this.handleCommand.bind(this);
	}

	/** Gets the path. */
	get path(): string {
		return this._path;
	}

	/** Gets the mode. */
	get mode(): 'view' | 'edit' {
		return this._mode;
	}

	/** Goes to the specified mode. */
	goToMode(mode: 'view' | 'edit'): void {
		this.app.setRouterQuery({
			mode
		}, { mergeOldQuery: true });
	}

	private _path: string = '';
	private _mode: 'view' | 'edit' = 'view';

	protected static override css = `
		.Shareable {
			min-height: 100%;
		}
		`;
}
