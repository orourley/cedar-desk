import { Params, Router, Sanitizer } from 'elm-app';
import { Page } from '../page';
import { FolderTree, ShareableForUser } from 'types';

import html from './shareable-page.html';
import css from './shareable-page.css';
import { Folder } from './shareables/folder';
import { Link } from './shareables/link';
import { Shareable } from './shareables/shareable';
import { NoteView } from './shareables/note-view';
import { NoteEdit } from './shareables/note-edit';
import { CheckListEdit } from './shareables/check-list-edit';
import { CheckListView } from './shareables/check-list-view';
import { RecipeView } from './shareables/recipe-view';
import { RecipeEdit } from './shareables/recipe-edit';

export class ShareablePage extends Page {

	constructor(params: Params) {
		super(params);

		// Bind functions to this.
		this.toggleFolderTree = this.toggleFolderTree.bind(this);
		this.goBack = this.goBack.bind(this);
		this.goUp = this.goUp.bind(this);
	}

	/** Cleans up the page. */
	protected override destroy(): void {

		// Remove any event listeners.
		this.app.removeEventListener('click', this.toggleFolderTree);

		// Remove from the broadcast group.
		if (this.path !== undefined && this.queryHas('.content .Shareable')) {
			this.app.unregisterServerHandler('shareable', this.queryComponent('.content .Shareable', Shareable).handleCommand);
			void this.app.send('broadcast', 'removeFromGroup', {
				group: `shareable-${this.path}`
			});
		}

		// Clear the toolbar buttons.
		this.app.clearToolbarIcons();
	}

	/** Process a URL query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the query variables.
		let path = query['path'];
		const publicViewPassword = query['publicViewPassword'];
		const mode = query['mode'];

		// If there's no public view password and the user is not logged in, go to the login page.
		if (this.app.username === undefined && publicViewPassword === undefined) {
			this.app.setRouterQuery({
				page: 'login'
			}, { mergeOldQuery: true });
			return;
		}

		// Remove the folder tree if not logged in.
		if (this.app.username === undefined) {
			this.root.classList.add('notLoggedIn');
		}

		// Check that the mode is valid.
		if (mode !== undefined && mode !== 'view' && mode !== 'edit') {
			const html = /* html */`<h1>Invalid mode.</h1>`;
			this.setHtml(html, this.query('.content', Element), this);
			return;
		}

		// Default to the user's main folder.
		if (path === undefined) {
			path = this.app.username;
		}

		// If still undefined (no user logged in), error.
		if (path === undefined) {
			const html = /* html */`<h1>No path was specified.</h1>`;
			this.setHtml(html, this.query('.content', Element), this);
			return;
		}

		// Remove the old path from the old broadcast group.
		if (this.path !== undefined) {
			await this.app.send('broadcast', 'removeFromGroup', {
				group: `shareable-${this.path}`
			});
		}
		if (this.queryHas('.content .Shareable')) {
			this.app.unregisterServerHandler('shareable', this.queryComponent('.content .Shareable', Shareable).handleCommand);
		}

		// Set the new path.
		this.path = path;

		// Get the shareable.
		let shareable: ShareableForUser;
		try {
			shareable = await this.app.send<ShareableForUser>('shareable', 'viewShareable', {
				path,
				publicViewPassword
			});
		}
		catch (error) {
			this.setHtml(`${error}`, this.query('.content', Element), this);
			return;
		}

		// Clean out the content in preparation for repopulating it.
		this.clearNode(this.query('.content', Element));

		// Set the title to the shareable's path or a shortened version of it.
		const shareablePathElem = this.query('.shareablePath', Element);
		const pathTokens = this.path.split('/');
		let html = '';
		for (let i = 0, l = pathTokens.length; i < l; i++) {
			const tokensJoined = pathTokens.filter((_value, index) => index <= i).join('/');
			html += `${i > 0 ? ' / ' : ''}<a onclick="goToPath|${Sanitizer.sanitizeForCallbackAttribute(tokensJoined)}">${pathTokens[i].replace(/ /g, '&nbsp;')}</a>`;
		}
		this.setHtml(html, shareablePathElem, this);
		shareablePathElem.scrollTo(shareablePathElem.scrollWidth, 0);

		// Get the shareable info.
		const shareableInfo = ShareablePage.shareableInfos[shareable.type];
		if (shareableInfo === undefined) {
			const html = /* html */`<h1>The type for this object is unknown.</h1>`;
			this.setHtml(html, this.query('.content', Element), this);
			return;
		}

		// Get the right mode for the shareable.
		const modeForShareable = mode ?? (shareable.permission === 'view' ? 'view' : shareableInfo.defaultMode);

		// If trying to edit, check the permissions.
		if (modeForShareable === 'edit' && shareable.permission === 'view') {
			if (this.app.username !== undefined) {
				const html = /* html */`<h1>You don't have permission to edit this object.</h1>`;
				this.setHtml(html, this.query('.content', Element), this);
			}
			else {
				this.app.setRouterQuery({
					page: 'login',
					prevPage: query['page']
				}, { mergeOldQuery: true });
			}
			return;
		}

		// Get the shareable type.
		const shareableType = shareableInfo[modeForShareable];
		if (shareableType === undefined) {
			const html = /* html */`<h1>The type for this object is unknown.</h1>`;
			this.setHtml(html, this.query('.content', Element), this);
			return;
		}

		// Set up the toolbar.
		this.app.clearToolbarIcons();
		this.app.addToolbarIcon('openFolderTree', 'left', 'assets/icons/menu.svg', 'Folder Tree', this.toggleFolderTree);
		this.app.addToolbarIcon('back', 'left', 'assets/icons/arrow-left.svg', 'Go Back', this.goBack);
		if (this.path.lastIndexOf('/') !== -1) {
			this.app.addToolbarIcon('goUp', 'left', 'assets/icons/arrow-up.svg', 'Go Up', this.goUp);
		}

		// Add the component, if it's there, and initialize it.
		const component = this.insertComponent(shareableType, this.query('.content', Element), undefined, {});
		try {
			component.setPathAndMode(path, modeForShareable);
			await component.initialize(shareable.content, query);
		}
		catch (error) {
			const html = /* html */`<h1>${(error as Error).message}</h1>`;
			const content = this.query('.content', Element);
			this.insertHtml(html, content, component, this);
			throw error;
		}

		// Add the path to the new broadcast group.
		this.app.registerServerHandler('shareable', this.queryComponent('.content .Shareable', shareableType).handleCommand);
		await this.app.send('broadcast', 'addToGroup', {
			group: `shareable-${this.path}`
		});

		await Promise.all([
			// Populate the sections.
			this.populateFolderTree()
		]);
	}

	/** Populate the folder tree. */
	private async populateFolderTree(): Promise<void> {

		// If there is no user logged in, show nothing.
		const username = this.app.username;
		if (username === undefined) {
			this.setHtml('', this.query('.folderTree', Element), this);
			return;
		}

		// Get the folder tree data and populate the html.
		try {
			const folderTree = await this.app.send<FolderTree>('shareable', 'getFolderTree', {});
			const html = this.getFolderTreeHtml(username, folderTree, 0);
			this.setHtml(html, this.query('.folderTree', HTMLElement), this);
		}
		catch (error) {
			const html = /* html */`<h1>${(error as Error).message}</h1>`;
			this.setHtml(html, this.query('.folderTree', Element), this);
		}
	}

	/** Get HTML for the folder tree, recursively. */
	private getFolderTreeHtml(parentPath: string, folderTree: FolderTree, depth: number): string {
		let html = '';
		if (depth === 0) {
			html += `
				<li class="folder" onclick="goToPath|${Sanitizer.sanitizeForCallbackAttribute(parentPath)}"><icon src="assets/icons/folder.svg" alt="folder"></icon> ${Sanitizer.sanitizeForHtml(this.app.username!)}</li>
				`;
		}
		if (folderTree.length > 0) {
			html += '<ol>';
			for (const item of folderTree) {
				html += `
					<li class="folder" onclick="goToPath|${Sanitizer.sanitizeForCallbackAttribute(`${parentPath}/${item.name}`)}"><icon src="assets/icons/folder.svg" alt="folder"></icon> ${Sanitizer.sanitizeForHtml(item.name)}</li>
					`;
				html += this.getFolderTreeHtml(`${parentPath}/${item.name}`, item.list, depth + 1);
			}
			html += '</ol>';
		}
		return html;
	}

	/** Goes to the specified path. */
	protected goToPath(path: string, event: Event): void {
		this.app.setRouterQuery({
			path,
			mode: undefined
		});
		event.preventDefault();
	}

	/** Goes up one path. */
	protected goUp(): void {

		if (this.path === undefined) {
			return;
		}

		// Get the parent path.
		const lastSlashIndex = this.path.lastIndexOf('/');
		if (lastSlashIndex === -1) {
			return;
		}
		const parentPath = this.path.substring(0, lastSlashIndex);

		// Do the router query.
		this.app.setRouterQuery({
			path: parentPath
		});
	}

	/** Goes back to the previous page. */
	protected goBack(): void {
		history.back();
	}

	/** Toggles the folder tree in mobile mode. */
	protected toggleFolderTree(event: Event): void {

		// Toggle the visible class.
		const folderTreeElem = this.query('.folderTree', HTMLElement);
		folderTreeElem.classList.toggle('visible');

		// Add or remove the event listener for closing it when the user clicks elsewhere.
		if (folderTreeElem.classList.contains('visible')) {
			this.app.addEventListener('click', this.toggleFolderTree);
		}
		else {
			this.app.removeEventListener('click', this.toggleFolderTree);
		}

		// Prevent the aforementioned event listener from triggering.
		event.stopPropagation();
	}

	/** The path of the shareable. */
	path: string | undefined;

	static override html = html;
	static override css = css;

	static shareableInfos: Record<string, { defaultMode: 'view' | 'edit', view?: new (params: Params) => Shareable, edit?: new (params: Params) => Shareable }> = {
		'folder': {
			defaultMode: 'view',
			view: Folder,
			edit: Folder
		},
		'link': {
			defaultMode: 'view',
			view: Link,
			edit: Link
		},
		'note': {
			defaultMode: 'edit',
			view: NoteView,
			edit: NoteEdit
		},
		'check-list': {
			defaultMode: 'edit',
			view: CheckListView,
			edit: CheckListEdit
		},
		'recipe': {
			defaultMode: 'view',
			view: RecipeView,
			edit: RecipeEdit
		}
	};
}
