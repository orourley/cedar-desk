const CopyWebpackPlugin = require('copy-webpack-plugin');
const TSConfigPathsWebpackPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

module.exports = {
	entry: './src/app.ts',
	output: {
		filename: 'script.js',
		clean: true
	},
	stats: 'minimal',
	resolve: {
		extensions: ['.ts', '.js'],
		plugins: [new TSConfigPathsWebpackPlugin()],
		alias: {
			'elm-app': path.resolve(__dirname, '../../elm-app'),
			'pine-lib': path.resolve(__dirname, '../../pine-lib'),
			'types': path.resolve(__dirname, '../types'),
			'oak-server': path.resolve(__dirname, '../server')
		}
	},
	module: {
		rules: [{
			test: /\.ts$/,
			loader: 'ts-loader'
		}, {
			test: /\.(css|svg|html)$/,
			use: 'raw-loader'
		}]
	},
	plugins: [
		new CopyWebpackPlugin({
			patterns: [{
				from: 'src/index.html'
			}, {
				from: 'src/config.json',
				noErrorOnMissing: true
			}, {
				from: 'src/manifest.json',
				noErrorOnMissing: true
			}, {
				from: 'src/assets',
				to: 'assets',
				noErrorOnMissing: true
			}]
		})
	]
};
