#!/usr/bin/env bash

# Setup:
# cd $HOME
# wget https://bootstrap.pypa.io/get-pip.py
# python3 get-pip.py
# python3 -m pip install boto3
# git clone https://gitlab.com/orourley/s3_sync.git
# cd s3_sync
# (copy key.txt file from home computer to this folder)

# Run this in cron as:
# 0 0 * * 0 $HOME/oak-server/backup.sh $HOME/oak-server/data

DATA_DIR=$1
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
DATE=`date -I'date'`

$SCRIPT_DIR/../s3_sync/s3_sync.py backup orourley-backup/oak-server/$DATE $DATA_DIR