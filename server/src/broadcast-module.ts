import { JsonHelper, JsonObject, JsonType } from 'pine-lib';
import { Connection, Module } from 'ironbark-server';
import { CedarDeskServer } from './cedar-desk-app';

/** A broadcast module for receiving and forwarding JSON to a group of connections. */
export class BroadcastModule extends Module<CedarDeskServer> {

	/** Processes a command. */
	onMessageReceived(command: string, params: JsonObject, connection: Connection): Promise<JsonType | void> {

		if (command === 'addToGroup') {
			return this.addToGroup(connection, params);
		}
		if (command === 'removeFromGroup') { // List the folder.
			return this.removeFromGroup(connection, params);
		}
		if (command === 'send') {
			return this.send(connection, params);
		}
		return Promise.resolve();
	}

	/** Add the connection to a broadcast group.
	 * { group: string } */
	private async addToGroup(connection: Connection, params: JsonObject): Promise<void> {

		// Get the group.
		const group = params['group'];
		if (!JsonHelper.isString(group)) {
			throw new Error('params.group must be a string.');
		}

		// Add to the broadcast group.
		this.server.addToBroadcastGroup(connection, group);
	}

	/** Remove the connection from a broadcast group.
	 * { group: string } */
	private async removeFromGroup(connection: Connection, params: JsonObject): Promise<void> {

		// Get the group.
		const group = params['group'];
		if (!JsonHelper.isString(group)) {
			throw new Error('params.group must be a string.');
		}

		// Remove from the broadcast gorup.
		this.server.removeFromBroadcastGroup(connection, group);
	}

	/** Send a message to a broadcast group.
	 * { group: string, json: JsonType } */
	private async send(connection: Connection, params: JsonObject): Promise<void> {

		// Get the username, if any.
		const username = this.server.users.getUsername(connection);
		if (username === undefined) {
			throw new Error(`You must be logged in to send broadcasts.`);
		}

		// Get the module.
		const module = params['module'];
		if (!JsonHelper.isString(module)) {
			throw new Error('params.module must be a string.');
		}

		// Get the group.
		const group = params['group'];
		if (!JsonHelper.isString(group)) {
			throw new Error('params.group must be a string.');
		}

		// Get the command.
		const command = params['command'];
		if (!JsonHelper.isString(command)) {
			throw new Error('params.command must be a string.');
		}

		// Get the json.
		const paramsOfCommand = params['params'];
		if (!JsonHelper.isObject(paramsOfCommand)) {
			throw new Error('params.params must be an object.');
		}

		// Remove from the broadcast gorup.
		this.server.broadcastJsonToGroup(connection, module, group, command, paramsOfCommand);
	}
}
