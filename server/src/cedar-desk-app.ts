import { ServerJson } from 'ironbark-server';
import { ShareableModule } from './shareable-module';
import { BroadcastModule } from './broadcast-module';

export class CedarDeskServer extends ServerJson {

	/** Initialize the server. Called right after the constructor. */
	protected override async initialize(): Promise<void> {
		await super.initialize();

		// Make it so any user can view the list of all users.
		this.users.setEnablAnyoneListAllUsers(true);

		// Register the modules.
		this.registerModule('broadcast', new BroadcastModule(this));
		this.registerModule('shareable', new ShareableModule(this));
	}
}

CedarDeskServer.start();
