import sharp = require('sharp');
import { Base64, JsonArray, JsonHelper, JsonObject, JsonType, Sort } from 'pine-lib';
import { Connection, Module } from 'ironbark-server';
import { Shareable, FolderItem, FolderTree, ShareableForUser, LinkContent, isFolder, isShareable, isLink, Permission, Permissions } from 'types';
import { CedarDeskServer } from './cedar-desk-app';

/** A generic shareable module. */
export class ShareableModule extends Module<CedarDeskServer> {

	/** Processes a command. */
	onMessageReceived(command: string, params: JsonObject, connection: Connection): Promise<JsonType | void> {

		// Get the username, if any.
		const username = this.server.users.getUsername(connection);

		if (command === 'getFolderTree') {
			return this.getFolderTree(username);
		}
		if (command === 'listFolder') { // List the folder.
			return this.listFolder(username, params);
		}
		if (command === 'viewShareable') {
			return this.viewShareable(username, params);
		}
		if (command === 'createShareable') {
			return this.createShareable(username, params);
		}
		if (command === 'deleteShareable') {
			return this.deleteShareable(connection, username, params);
		}
		if (command === 'transferShareable') {
			return this.transferShareable(connection, username, params);
		}
		if (command === 'starShareable') {
			return this.starShareable(username, params);
		}
		if (command === 'getType') {
			return this.getType(username, params);
		}
		if (command === 'getPermissions') {
			return this.getPermissions(username, params);
		}
		if (command === 'getPermission') {
			return this.getPermission(username, params);
		}
		if (command === 'updatePermissions') {
			return this.updatePermissions(username, params);
		}
		if (command === 'updatePublicViewPassword') {
			return this.updatePublicViewPassword(username, params);
		}
		if (command === 'updateShareableContent') {
			return this.updateShareableContent(connection, username, params);
		}
		if (command === 'setField') {
			return this.setField(connection, username, params);
		}
		if (command === 'insertField') {
			return this.insertField(connection, username, params);
		}
		if (command === 'convertToResizedWebpBase64') {
			return this.convertToResizedWebpBase64(username, params);
		}
		return Promise.resolve();
	}

	/** Get the folder tree of the logged in user. If there is none, create it.
	 * { } */
	private async getFolderTree(username: string | undefined): Promise<FolderTree> {

		// Check that there is a username.
		if (username === undefined) {
			throw new Error(`You must be logged in to view your folder tree.`);
		}

		// Get the data.
		const folderTreeNode = await this.server.dataStore.getJson<FolderTree>(`shareables/${username}/__.tree.json`);

		// If there is none, create the user and its empty folder tree.
		if (folderTreeNode === undefined) {

			await this.server.dataStore.setJson(`shareables/${username}.json`, {
				name: username,
				permissions: {},
				publicView: undefined,
				type: 'folder',
				content: {
					list: []
				}
			});

			await this.server.dataStore.setJson(`shareables/${username}/__.tree.json`, []);
		}

		// Return the folder tree, or an empty tree if there is none.
		return folderTreeNode ?? [];
	}

	/** Lists a folder.
	 * { path: string, publicViewPassword: string | undefined }	*/
	private async listFolder(username: string | undefined, params: JsonObject): Promise<FolderItem[]> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the publicViewPassword.
		const publicViewPassword = params['publicViewPassword'];
		if (!JsonHelper.isString(publicViewPassword) && publicViewPassword !== undefined) {
			throw new Error(`The publicViewPassword must be a string or undefined.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, publicViewPassword);
		if (permission === 'none') {
			throw new Error(`You don't have permission to view this folder.`);
		}

		// Get the shareable.
		const shareable = await this.getShareable(path);

		// Check that it is a folder.
		if (!isFolder(shareable)) {
			throw new Error(`The path ${path} is not a folder.`);
		}

		// Return the file list.
		return shareable.content.list;
	}

	/** Views a shareable.
	 * { path: string, publicViewPassword: string | undefined }	*/
	private async viewShareable(username: string | undefined, params: JsonObject): Promise<ShareableForUser> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the publicViewPassword.
		const publicViewPassword = params['publicViewPassword'];
		if (!JsonHelper.isString(publicViewPassword) && publicViewPassword !== undefined) {
			throw new Error(`The publicViewPassword must be a string or undefined.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, publicViewPassword);
		if (permission === 'none') {
			throw new Error(`You don't have permission to view this object.`);
		}

		// Get the shareable.
		const shareable = await this.getShareable(path);

		// Return the file.
		return {
			name: shareable.name,
			type: shareable.type,
			content: shareable.content,
			permission
		};
	}

	/** Creates a new shareable. Keeps the same permissions as the parent.
	 * { path: string, name: string, type: string, content: JsonObject } */
	private async createShareable(username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the name of the new shareable.
		const name = params['name'];
		if (!JsonHelper.isString(name)) {
			throw new Error(`The name must be a string.`);
		}
		if (name === '') {
			throw new Error(`The name must not be an empty string.`);
		}
		if (name.includes('/')) {
			throw new Error(`The name cannot include a slash.`);
		}

		// Get the type of the new shareable.
		const type = params['type'];
		if (!JsonHelper.isString(type)) {
			throw new Error(`The type must be a string.`);
		}
		if (type.length === 0) {
			throw new Error(`The type must not be an empty string.`);
		}

		// Get the content of the new shareable.
		const content = params['content'];
		if (!JsonHelper.isObject(content)) {
			throw new Error(`The content must be a JSON object.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'edit' && permission !== 'full') {
			throw new Error(`You don't have permission to create anything in this folder.`);
		}

		// Create the shareable.
		const newShareable: Shareable = {
			name,
			type,
			permissions: {},
			publicViewPassword: undefined,
			content
		};

		// For special types, make sure they work correctly.
		if (type === 'folder') {

			if (!JsonHelper.hasProp(newShareable.content, 'list', (value): value is [] => JsonHelper.isArray(value) && value.length === 0)) {
				throw new Error(`A new folder must have content { list: [] }.`);
			}
		}
		else if (type === 'link') {
			if (!JsonHelper.hasStringProp(newShareable.content, 'path') || !JsonHelper.hasStringProp(newShareable.content, 'type')) {
				throw new Error(`A new link must have content { path: string, type: string }.`);
			}
			if (newShareable.content.type === 'link') {
				throw new Error('A link cannot link to a link.');
			}
		}

		// Add the shareable.
		await this.addShareable(path, newShareable);
	}

	/** Removes a shareable.
	 * { path: string } */
	private async deleteShareable(connection: Connection, username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'full') {
			throw new Error(`You don't have permission to remove this.`);
		}

		// Remove the shareable.
		await this.removeShareable(path, false);

		// Broadcast the change.
		this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${path}`, 'deleteShareable', {
			path
		});
	}

	/** Transfers a shareable.
	 * { path: string, newFolder: string, newName: string, keepOrig: boolean } */
	private async transferShareable(connection: Connection, username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path of the original.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path param must be a string.`);
		}

		// Get the new path of the shareable to remove.
		const newFolder = params['newFolder'];
		if (!JsonHelper.isString(newFolder)) {
			throw new Error(`The newFolder param must be a string.`);
		}

		// Get the new name of the new shareable.
		const newName = params['newName'];
		if (!JsonHelper.isString(newName)) {
			throw new Error(`The newName param must be a string.`);
		}
		if (newName === '') {
			throw new Error(`The new name must not be an empty string.`);
		}
		if (newName.includes('/')) {
			throw new Error(`The new name cannot include a slash.`);
		}

		// Get if we're keeping the original.
		const keepOrig = params['keepOrig'];
		if (!JsonHelper.isBoolean(keepOrig)) {
			throw new Error(`The keepOrig param must be a boolean.`);
		}

		// Return if there's no actual change in name.
		const newPath = `${newFolder}/${newName}`;
		if (path === newPath) {
			return;
		}

		// Verify permission on the path.
		const permission = await this.getShareablePermission(username, path, undefined);
		if ((keepOrig && permission === 'none') || permission !== 'full' ) {
			throw new Error(`You don't have permission to transfer ${path}.`);
		}

		// Verify permission on the new path.
		const newPermission = await this.getShareablePermission(username, newPath, undefined);
		if (newPermission !== 'full') {
			throw new Error(`You don't have permission to add ${newPath}.`);
		}

		// Get the shareable to move.
		const shareable = await this.getShareable(path);

		// Set its new name.
		shareable.name = newName;

		// If the shareable is a folder, transfer its files over.
		if (isFolder(shareable)) {

			// Check if the new folder is inside the old folder.
			if ((newPath + '/').startsWith(path + '/')) {
				throw new Error('A folder cannot be moved inside itself!');
			}

			// Make the shareable content list empty.
			const items = shareable.content.list;
			shareable.content.list = [];

			// Add the shareable to the new path.
			await this.addShareable(newFolder, shareable);

			// Transfer all of the folder's files as well.
			for (const item of items) {
				await this.transferShareable(connection, username, {
					path: `${path}/${item.name}`,
					newFolder: newPath,
					newName: item.name,
					keepOrig: keepOrig
				});
			}
		}
		else {
			// Add the shareable to the new path.
			await this.addShareable(newFolder, shareable);
		}

		// Broadcast the change to both the new and old paths.
		this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${newPath}`, 'addShareable', {
			path,
			newPath
		});

		// If the keepOrig flag isn't set, remove it from the old path.
		if (!keepOrig) {
			await this.removeShareable(path, false);

			this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${path}`, 'removeShareable', {
				path,
				newPath
			});
		}
	}

	/** Updates the permissions of a shareable.
	 * { folder: string, name: string, starred: boolean } */
	private async starShareable(username: string | undefined, params: JsonObject): Promise<void> {

		// Get the folder of the shareable.
		const folder = params['folder'];
		if (!JsonHelper.isString(folder)) {
			throw new Error(`The folder must be a string.`);
		}

		// Get the name of the shareable.
		const name = params['name'];
		if (!JsonHelper.isString(name)) {
			throw new Error(`The name must be a string.`);
		}

		// Get the starred flag of the shareable.
		const starred = params['starred'];
		if (!JsonHelper.isBoolean(starred)) {
			throw new Error(`The starred flag must be a boolean.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, folder, undefined);
		if (permission !== 'edit' && permission !== 'full') {
			throw new Error(`You don't have permission to star or unstar an object in ${folder}.`);
		}

		// Modify the file.
		await this.modifyShareable(folder, async (shareable) => {

			// Check that it is a folder.
			if (!isFolder(shareable)) {
				throw new Error(`The path ${folder} is not a folder.`);
			}

			// Update the permissions.
			const itemIndex = shareable.content.list.findIndex((folderItem) => folderItem.name === name);
			if (itemIndex === -1) {
				throw new Error(`The folder ${folder} does not have an item ${name}.`);
			}

			// Set the starred status.
			shareable.content.list[itemIndex].starred = starred;

			// Sort the list.
			Sort.sort(shareable.content.list, this.isLess);

			// Return the data.
			return shareable;
		});
	}

	/** Gets the permission of a shareable.
	 * { path, publicView: { salt: string, hash: string } | undefined } */
	private async getType(username: string | undefined, params: JsonObject): Promise<string> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the publicViewPassword.
		const publicViewPassword = params['publicViewPassword'];
		if (!JsonHelper.isString(publicViewPassword) && publicViewPassword !== undefined) {
			throw new Error(`The publicViewPassword must be a string or undefined.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, publicViewPassword);
		if (permission === 'none') {
			throw new Error(`You don't have permission to view the type of this file.`);
		}

		// Get the shareable to move.
		const shareable = await this.getShareable(path);

		// Return the permission.
		return shareable.type;
	}

	/** Gets the permission of a shareable.
	 * { path } */
	private async getPermissions(username: string | undefined, params: JsonObject): Promise<Permissions> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'full') {
			throw new Error(`You don't have permission to get the permissions.`);
		}

		// Get the data.
		const shareable = await this.getShareable(path);

		// Return the permissions.
		return {
			permissions: shareable.permissions,
			publicViewPassword: shareable.publicViewPassword
		};
	}

	/** Gets the permission of a shareable.
	 * { path, publicView: { salt: string, hash: string } | undefined } */
	private async getPermission(username: string | undefined, params: JsonObject): Promise<'none' | 'view' | 'edit' | 'full'> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the publicViewPassword.
		const publicViewPassword = params['publicViewPassword'];
		if (!JsonHelper.isString(publicViewPassword) && publicViewPassword !== undefined) {
			throw new Error(`The publicViewPassword must be a string or undefined.`);
		}

		// Return the permission.
		return await this.getShareablePermission(username, path, publicViewPassword);
	}

	/** Updates the permissions of a shareable.
	 * { path, permissions: Record<string, 'none' | 'view' | 'edit' | 'full'> } */
	private async updatePermissions(username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the permissions.
		const permissions = params['permissions'];
		if (!JsonHelper.isObjectOfV(permissions, (value): value is Permission => {
			return value === 'none' || value === 'view' || value === 'edit' || value === 'full';
		})) {
			throw new Error(`The permissions must be an object.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'full') {
			throw new Error(`You don't have permission to update the permissions.`);
		}

		// Modify the file.
		await this.modifyShareable(path, async (shareable) => {

			// Update the permissions.
			shareable.permissions = permissions;

			// Return the data.
			return shareable;
		});
	}

	/** Updates the public view password of a shareable.
	 * { path, publicViewPassword: string | false | undefined } */
	private async updatePublicViewPassword(username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the public view password.
		const publicViewPassword = params['publicViewPassword'];
		if (!JsonHelper.isString(publicViewPassword) && publicViewPassword !== false && publicViewPassword !== undefined) {
			throw new Error(`The publicViewPassword must be a string, false, or undefined.`);
		}
		if (JsonHelper.isString(publicViewPassword) && publicViewPassword === '') {
			throw new Error(`The publicViewPassword must not be an empty string.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'full') {
			throw new Error(`You don't have permission to update the permissions.`);
		}

		// Modify the file.
		await this.modifyShareable(path, async (shareable) => {

			// Update the public view settings.
			shareable.publicViewPassword = publicViewPassword;

			// Return the data.
			return shareable;
		});
	}

	/** Updates a shareable's content.
	 * { path: string, content: JsonObject } */
	private async updateShareableContent(connection: Connection, username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the updated content.
		const updatedContent = params['content'];
		if (!JsonHelper.isObject(updatedContent)) {
			throw new Error(`The content must be an object.`);
		}

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'edit' && permission !== 'full') {
			throw new Error(`You don't have permission to edit this.`);
		}

		// Modify the file.
		await this.modifyShareable(path, async (shareable) => {

			// If it's a folder or link, we update them differently.
			if (isFolder(shareable)) {
				throw new Error(`Cannot update a folder.`);
			}
			if (isLink(shareable)) {
				throw new Error(`Cannot update a link.`);
			}

			// Update the content.
			shareable.content = updatedContent;

			// Return the data.
			return shareable;
		});

		// Broadcast the change.
		this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${path}`, 'updateShareableContent', {
			path
		});
	}

	/** Sets the value of a field within a shareable. If the value is undefined, the field is deleted.
	 * Broadcasts to all others in the path the same update.
	 * { path: string, field: string, value: JsonType | undefined } */
	private async setField(connection: Connection, username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the field.
		const field = params['field'];
		if (!JsonHelper.isString(field)) {
			throw new Error(`The field must be a string.`);
		}

		// Get the value.
		// No checking here, because it should only be the same type as existing value.
		const value = params['value'];

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'edit' && permission !== 'full') {
			throw new Error(`You don't have permission to edit this.`);
		}

		// Modify the file.
		await this.modifyShareable(path, async (shareable) => {

			// If it's a folder, we need to first remove the children.
			if (isFolder(shareable)) {
				throw new Error(`Cannot update a folder.`);
			}

			// Get the property.
			const result = this.getPropertyOfField(field, shareable.content);
			const existingValue = result.get();
			if (value !== undefined) {
				if (typeof existingValue !== typeof value
					|| JsonHelper.isObject(existingValue) !== JsonHelper.isObject(value)
					|| JsonHelper.isArray(existingValue) !== JsonHelper.isArray(value)) {
					throw new Error(`Cannot change the type of an existing value (${typeof existingValue} -> ${typeof value}).`);
				}
			}

			// Set the actual value.
			result.set(value);

			// Return the data.
			return shareable;
		});

		// Broadcast the change.
		this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${path}`, 'setField', {
			path,
			field,
			value
		});
	}

	/** Inserts value into an object or array field in a shareable.
	 * Broadcasts to all others in the path the same update.
	 * { path: string, field: string, value: JsonType | undefined } */
	private async insertField(connection: Connection, username: string | undefined, params: JsonObject): Promise<void> {

		// Get the path.
		const path = params['path'];
		if (!JsonHelper.isString(path)) {
			throw new Error(`The path must be a string.`);
		}

		// Get the field.
		const field = params['field'];
		if (!JsonHelper.isString(field)) {
			throw new Error(`The field must be a string.`);
		}

		// Get the value.
		// No checking here, because it should only be the same type as existing value.
		const value = params['value'];

		// Verify permission.
		const permission = await this.getShareablePermission(username, path, undefined);
		if (permission !== 'edit' && permission !== 'full') {
			throw new Error(`You don't have permission to edit this.`);
		}

		// Modify the file.
		await this.modifyShareable(path, async (shareable) => {

			// If it's a folder, we need to first remove the children.
			if (isFolder(shareable)) {
				throw new Error(`Cannot update a folder.`);
			}

			// Get the property.
			const result = this.getPropertyOfField(field, shareable.content);

			// Set the actual value.
			result.insert(value);

			// Return the data.
			return shareable;
		});

		// Broadcast the change.
		this.server.broadcastJsonToGroup(connection, 'shareable', `shareable-${path}`, 'insertField', {
			path,
			field,
			value
		});
	}

	/** Gets the [parent, property, value] of the field to set. */
	private getPropertyOfField(field: string, content: JsonObject | JsonArray): ObjectAndProperty | ArrayAndProperty {
		const tokens = field.split('.');
		let i = 0;
		for (let l = tokens.length - 1; i < l; i++) {
			let nextContent;
			let tokenAsNum;
			if (JsonHelper.isArray(content)) {
				if (tokens[i].match(/^\d$/) !== null) {
					tokenAsNum = parseInt(tokens[i]);
				}
				else {
					tokenAsNum = content.findIndex((value): value is JsonObject => JsonHelper.isObject(value) && value['id'] === tokens[i]);
					if (tokenAsNum === -1) {
						throw new Error(`The field "${tokens[i]}" in the array ${tokens.slice(0, i).join('.')} must be an integer or a string id.`);
					}
				}
				nextContent = content[tokenAsNum];
			}
			else {
				nextContent = content[tokens[i]];
			}
			if (!JsonHelper.isObject(nextContent) && !JsonHelper.isArray(nextContent)) {
				throw new Error(`The field ${tokens.slice(0, i + 1).join('.')} must already exist as an object or array.`);
			}
			content = nextContent;
		}
		if (JsonHelper.isArray(content)) {
			if (tokens[i].match(/^\d$/) !== null) {
				return new ArrayAndProperty(content, parseInt(tokens[i]));
			}
			else {
				return new ArrayAndProperty(content, tokens[i]);
			}
		}
		else {
			return new ObjectAndProperty(content, tokens[i]);
		}
	}

	/** Resizes and converts an image to a base64 string of a webp image.
	 * { image: string, maxSize: number } */
	private async convertToResizedWebpBase64(username: string | undefined, params: JsonObject): Promise<string> {

		// Check that there is a username.
		if (username === undefined) {
			throw new Error(`You must be logged in to convert images.`);
		}

		// Get the image binary.
		const image = params['image'];
		if (!JsonHelper.isString(image)) {
			throw new Error('params.image must be a string.');
		}

		// Get the max size for the new image.
		const maxSize = params['maxSize'];
		if (!JsonHelper.isNumber(maxSize)) {
			throw new Error('params.maxSize must be a number.');
		}

		// Convert the image to an array buffer.
		const imageData = Base64.toArrayBuffer(image);

		// Do the resizing and return the base64 webp string.
		return await this.resizeToBase64WebP(imageData, 1024);
	}

	private async resizeToBase64WebP(data: ArrayBuffer, maxSize: number): Promise<string> {

		const resizedBuffer = await sharp(Buffer.from(data)).resize(maxSize, maxSize, { fit: 'inside' }).rotate().webp().toBuffer();

		return Base64.toBase64(resizedBuffer);
	}

	/** Adds a shareable to a folder. */
	private async addShareable(parentFolder: string, shareable: Shareable): Promise<void> {

		// Modify the folder that will contain the shareable.
		await this.modifyShareable(parentFolder, async (shareableFolder) => {

			// Check that it is a folder.
			if (!isFolder(shareableFolder)) {
				throw new Error(`The path ${parentFolder} is not a folder.`);
			}

			// Make sure it doesn't already exist.
			if (shareableFolder.content.list.findIndex((value) => value.name === shareable.name) !== -1) {
				throw new Error(`The name ${shareable.name} already exists in this folder.`);
			}

			// Add the shareable to the folder, sorted.
			Sort.add({
				name: shareable.name,
				type: shareable.type === 'link' ? (shareable.content as LinkContent).type : shareable.type,
				isLink: shareable.type === 'link',
				starred: false
			}, shareableFolder.content.list, this.isLess);

			// Save the shareable.
			await this.server.dataStore.setJson(`shareables/${parentFolder}/${shareable.name}.json`, shareable);

			// Modify the folder tree for the user of the path.
			if (shareable.type === 'folder') {
				const ownerOfShareable = this.getOwnerOfShareable(parentFolder);
				await this.server.dataStore.modifyJson(`shareables/${ownerOfShareable}/__.tree.json`, async (tree: undefined | FolderTree) => {

					// If there's no tree yet, make it.
					if (tree === undefined) {
						tree = [];
					}

					// Go through each level of the tree, adding the folders as necessary.
					let treeChild = tree;
					const pathTokens = `${parentFolder}/${shareable.name}`.split('/');
					for (let i = 1, l = pathTokens.length; i < l; i++) { // Don't do the first token, ownerOfShareable.
						let indexInParent: number = treeChild.findIndex((value) => value.name === pathTokens[i]);
						if (indexInParent === -1) {
							indexInParent = Sort.add({ name: pathTokens[i], list: [], }, treeChild, (lhs, rhs) => lhs.name < rhs.name);
						}
						treeChild = treeChild[indexInParent].list;
					}

					// Return the data.
					return tree;
				});
			}

			// Return the updated folder.
			return shareableFolder;
		});
	}

	/** Removes a shareable from a folder. */
	private async removeShareable(path: string, recursing: boolean): Promise<void> {

		// Modify the shareable.
		await this.modifyShareable(path, async (shareable) => {

			// If it's a folder,
			if (isFolder(shareable)) {

				// We need to first remove all of the children, recursively.
				const removePromises = [];
				for (const item of shareable.content.list) {
					removePromises.push(this.removeShareable(`${path}/${item.name}`, true));
				}
				await Promise.all(removePromises);

				// Remove it from the folder tree.
				const ownerOfShareable = this.getOwnerOfShareable(path);
				await this.server.dataStore.modifyJson(`shareables/${ownerOfShareable}/__.tree.json`, async (tree: undefined | FolderTree) => {

					// If there's no tree yet, make it.
					if (tree === undefined) {
						tree = [];
					}

					// Go through each level of the tree, removing the folders as necessary.
					let treeChild = tree;
					const pathTokens = path.split('/');
					for (let i = 1, l = pathTokens.length; i < l; i++) { // Don't do the first token, ownerOfShareable.
						const indexInParent: number = treeChild.findIndex((value) => value.name === pathTokens[i]);
						if (indexInParent === -1) {
							break;
						}
						if (i === l - 1) {
							treeChild.splice(indexInParent, 1);
							break;
						}
						treeChild = treeChild[indexInParent].list;
					}

					// Return the data.
					return tree;
				});
			}

			// If we're recursing (removing parent also), don't bother with the parent.
			if (recursing) {
				return undefined;
			}

			// Remove it from the list of the parent folder.
			const indexOfLastSlash = path.lastIndexOf('/');
			const parentFolder = path.substring(0, indexOfLastSlash);
			const name = path.substring(indexOfLastSlash + 1);
			await this.modifyShareable(parentFolder, async (shareable) => {

				// Make sure that it's a folder.
				if (!isFolder(shareable)) {
					throw new Error(`Parent folder is not a folder, something really broke.`);
				}

				// Get the index of the shareable to be removed.
				const indexToRemove = shareable.content.list.findIndex((value) => value.name === name);
				if (indexToRemove === -1) {
					throw new Error(`The name ${name} does not exist under ${path}. Something really broke.`);
				}

				// Remove it from the list.
				shareable.content.list.splice(indexToRemove, 1);

				return shareable;
			});

			// Return undefined to delete the shareable.
			return undefined;
		});
	}

	/** Gets a shareable. */
	private async getShareable(path: string): Promise<Shareable> {

		// Get the data.
		const shareable = await this.server.dataStore.getJson(`shareables/${path}.json`);

		// Check that it is a valid shareable.
		if (shareable === undefined) {
			throw new Error(`The path ${path} is not found.`);
		}
		if (!isShareable(shareable)) {
			throw new Error(`The path ${path} is not valid.`);
		}

		// Return it.
		return shareable;
	}

	/** Modifies a shareable. They must be the owner. If func returns undefined, the shareable is deleted. */
	private async modifyShareable(path: string, func: (shareable: Shareable) => Promise<Shareable | undefined>): Promise<void> {

		// Get the data.
		await this.server.dataStore.modifyJson(`shareables/${path}.json`, async (shareable) => {

			// Check that it is a valid shareable.
			if (shareable === undefined) {
				throw new Error(`The path ${path} is not found.`);
			}
			if (!isShareable(shareable)) {
				throw new Error(`The path ${path} is not valid.`);
			}

			// Do something to the shareable.
			shareable = await func(shareable);

			// Return the shareable.
			return shareable;
		});
	}

	/** Returns true if the shareable has permission to be viewed by the user. */
	private async getShareablePermission(username: string | undefined, path: string, publicViewPassword: string | undefined): Promise<Permission> {

		// Get the owner of the shareable.
		const ownerOfShareable = this.getOwnerOfShareable(path);

		// Return 'full' if the user owns the shareable, the common case.
		if (ownerOfShareable === username) {
			return 'full';
		}

		// Return 'full' if the user is an admin.
		if (username !== undefined && await this.server.users.isAdmin(username, {})) {
			return 'full';
		}

		// Keep moving up in folders until an explicit permission is given.
		let firstToken = true;
		while (true) {

			// Get the data.
			const shareable = await this.server.dataStore.getJson(`shareables/${path}.json`);

			// Make sure it's a valid shareable.
			if (shareable !== undefined && isShareable(shareable)) {

				if (username !== undefined) {

					// Get the permission.
					let permission = shareable.permissions[username];

					// If there is no permission, see if there is one for all users.
					if (permission === undefined) {
						permission = shareable.permissions['*'];
					}

					// Return 'full' if the permission is 'full'
					if (permission === 'full') {
						return 'full';
					}

					// Return 'full' if this is an ancestor folder and it is 'edit'.
					if (!firstToken && permission === 'edit') {
						return 'full';
					}

					// Return the permission if the permission list the user is defined.
					if (permission !== undefined) {
						return permission;
					}
				}

				// Check the public view permissions.
				if (shareable.publicViewPassword === false) {
					if (username === undefined) {
						return 'none';
					}
				}
				else if (shareable.publicViewPassword !== undefined && shareable.publicViewPassword === publicViewPassword) {
					return 'view';
				}
			}

			// Move up a folder.
			path = path.substring(0, path.lastIndexOf('/'));

			// If empty, we're at the root folder, so no permissions.
			if (path === '') {
				break;
			}

			firstToken = false;
		}

		// No permission.
		return 'none';
	}

	/** Gets the owner of the shareable. */
	private getOwnerOfShareable(path: string): string {
		const indexOfSlash = path.indexOf('/');
		if (indexOfSlash !== -1) {
			return path.slice(0, indexOfSlash);
		}
		else {
			return path;
		}
	}

	/** An isLess function for sorting shareables in a folder. */
	private isLess(lhs: FolderItem, rhs: FolderItem): boolean {
		if (lhs.starred && !rhs.starred) {
			return true;
		}
		if (!lhs.starred && rhs.starred) {
			return false;
		}
		if (lhs.type === 'folder' && rhs.type !== 'folder') {
			return true;
		}
		if (lhs.type !== 'folder' && rhs.type === 'folder') {
			return false;
		}
		return lhs.name < rhs.name;
	}
}

/** A an array and property pair.
 * The property can be an id, in which case the searched for item in the array is { id: property }. */
class ArrayAndProperty {
	parent: JsonArray;
	property: number | string;
	constructor(parent: JsonArray, property: number | string) {
		this.parent = parent;
		this.property = property;
	}
	get(): JsonType | undefined {
		let propertyIndex;
		if (typeof this.property === 'string') {
			propertyIndex = this.parent.findIndex((value): value is JsonObject => JsonHelper.isObject(value) && value['id'] === this.property);
		}
		else {
			propertyIndex = this.property;
		}
		return this.parent[propertyIndex];
	}
	set(value: JsonType | undefined): void {
		let propertyIndex;
		if (typeof this.property === 'string') {
			if (this.property !== '') {
				propertyIndex = this.parent.findIndex((value): value is JsonObject => JsonHelper.isObject(value) && value['id'] === this.property);
				if (propertyIndex === -1) {
					throw new Error(`The property is an invalid id.`);
				}
			}
			else {
				propertyIndex = this.parent.length;
			}
		}
		else {
			propertyIndex = this.property;
			if (propertyIndex < 0 || propertyIndex >= this.parent.length) {
				throw new Error(`The index is invalid.`);
			}
		}
		if (value !== undefined) {
			this.parent[propertyIndex] = value;
		}
		else {
			this.parent.splice(propertyIndex, 1);
		}
	}
	insert(value: JsonType | undefined): void {
		if (value !== undefined) {
			let propertyIndex;
			if (typeof this.property === 'string') {
				if (this.property !== '') {
					propertyIndex = this.parent.findIndex((value): value is JsonObject => JsonHelper.isObject(value) && value['id'] === this.property);
					if (propertyIndex === -1) {
						throw new Error(`The property is an invalid id.`);
					}
				}
				else {
					propertyIndex = this.parent.length;
				}
			}
			else {
				propertyIndex = this.property;
				if (propertyIndex < 0 || propertyIndex > this.parent.length) {
					throw new Error(`The index is invalid.`);
				}
			}
			this.parent.splice(propertyIndex, 0, value);
		}
	}
}

class ObjectAndProperty {
	parent: JsonObject;
	property: string;
	constructor(parent: JsonObject, property: string) {
		this.parent = parent;
		this.property = property;
	}
	get(): JsonType | undefined {
		return this.parent[this.property];
	}
	set(value: JsonType | undefined): void {
		if (this.parent[this.property] === undefined) {
			throw new Error(`The property is invalid.`);
		}
		if (value !== undefined) {
			this.parent[this.property] = value;
		}
		else {
			delete this.parent[this.property];
		}
	}
	insert(value: JsonType | undefined): void {
		if (this.parent[this.property] !== undefined) {
			throw new Error(`The property is invalid.`);
		}
		if (value !== undefined) {
			this.parent[this.property] = value;
		}
	}
}
