import { JsonObject } from 'pine-lib';

/*
A list of lists that is specific to users.
*/

export interface CheckListListItem extends JsonObject {
	id: string, // The unique id for the list.
	title: string // The title of the list.
}

/** A list of check-list items. */
export type CheckListListData = CheckListListItem[];

/** A check-list item. */
export interface CheckListItem extends JsonObject {
	id: string,
	checked: boolean,
	text: string,
	level: number
}

/** A check-list. */
export type CheckListContent = {
	id: string, // A unique id.
	title: string, // The title of the list.
	users: string[], // The list of users that have this check-list in their check-list list.
	removeOnCheck: boolean, // When the user checks, does it remove the item?
	toggleDrag: boolean, // Can the user always drag or is there a toggle button?
	items: CheckListItem[]; // The items in the check-list.
}

export const newCheckListContent: CheckListContent = {
	id: '',
	title: '',
	users: [],
	removeOnCheck: false,
	toggleDrag: false,
	items: []
};
