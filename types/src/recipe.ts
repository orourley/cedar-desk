export type VolumeUnit = 'mL' | 'L' | 'pinch' | 'tsp' | 'tbsp' | 'fl oz' | 'cup' | 'pint' | 'qt' | 'gal';
export type WeightUnit = 'mg' | 'g' | 'kg' | 'lb' | 'oz';
export type LengthUnit = 'mm' | 'cm' | 'm' | 'in' | 'ft' | 'yd';
export type TemperatorUnit = '°F' | '°C';
export type TimeUnit = 'sec' | 'min' | 'hr';

export type Ingredient = {
	amount: string,
	unit: string, // VolumeUnit | WeightUnit | LengthUnit;
	text: string,
	heading: boolean
}

export type Step = {
	text: string,
	heading: boolean
}

export type Note = {
	text: string,
	heading: boolean
}

export type Image = {
	id?: string,
	data: string,
	caption: string
}

/** A recipe. */
export type RecipeContent = {
	description: string,
	makesAmount: number | undefined,
	makesUnit: string,
	tools: string[],
	ingredients: Ingredient[],
	steps: Step[],
	notes: Note[],
	images: Image[]
}

export const newRecipeContent: RecipeContent = {
	description: '',
	makesAmount: 1,
	makesUnit: 'serving',
	tools: [],
	ingredients: [],
	steps: [],
	notes: [],
	images: []
};

/** Convert from one volume unit to another. */
export function convertVolume(value: number, fromUnit: VolumeUnit, toUnit: VolumeUnit): number {
	let volumeInML: number;
	switch (fromUnit) {
		case 'mL': volumeInML = value * 1; break;
		case 'L': volumeInML = value * 1000; break;
		case 'pinch': volumeInML = value * 0.492892; break;
		case 'tsp': volumeInML = value * 4.92892; break;
		case 'tbsp': volumeInML = value * 14.7868; break;
		case 'fl oz': volumeInML = value * 29.5735; break;
		case 'cup': volumeInML = value * 236.588; break;
		case 'pint': volumeInML = value * 473.176; break;
		case 'qt': volumeInML = value * 946.353; break;
		case 'gal': volumeInML = value * 3785.41; break;
	}
	switch (toUnit) {
		case 'mL': return volumeInML / 1;
		case 'L': return volumeInML / 1000;
		case 'pinch': return volumeInML / 0.492892;
		case 'tsp': return volumeInML / 4.92892;
		case 'tbsp': return volumeInML / 14.7868;
		case 'fl oz': return volumeInML / 29.5735;
		case 'cup': return volumeInML / 236.588;
		case 'pint': return volumeInML / 473.176;
		case 'qt': return volumeInML / 946.353;
		case 'gal': return volumeInML / 3785.41;
	}
}

/** Convert from one weight unit to another. */
export function convertWeight(value: number, fromUnit: WeightUnit, toUnit: WeightUnit): number {
	let weightInMG: number;
	switch (fromUnit) {
		case 'mg': weightInMG = value * 1; break;
		case 'g': weightInMG = value * 1000; break;
		case 'kg': weightInMG = value * 1000000; break;
		case 'lb': weightInMG = value * 453592; break;
		case 'oz': weightInMG = value * 28349.5; break;
	}
	switch (toUnit) {
		case 'mg': return weightInMG / 1;
		case 'g': return weightInMG / 1000;
		case 'kg': return weightInMG / 1000000;
		case 'lb': return weightInMG / 453592;
		case 'oz': return weightInMG / 28349.5;
	}
}

/** Convert from one length unit to another. */
export function convertLength(value: number, fromUnit: LengthUnit, toUnit: LengthUnit): number {
	let lengthInMM: number;
	switch (fromUnit) {
		case 'mm': lengthInMM = value * 1; break;
		case 'cm': lengthInMM = value * 10; break;
		case 'm': lengthInMM = value * 1000; break;
		case 'in': lengthInMM = value * 25.4; break;
		case 'ft': lengthInMM = value * 304.8; break;
		case 'yd': lengthInMM = value * 914.4; break;
	}
	switch (toUnit) {
		case 'mm': return lengthInMM / 1;
		case 'cm': return lengthInMM / 10;
		case 'm': return lengthInMM / 1000;
		case 'in': return lengthInMM / 25.4;
		case 'ft': return lengthInMM / 304.8;
		case 'yd': return lengthInMM / 914.4;
	}
}

/** Convert from one time unit to another. */
export function convertTime(value: number, fromUnit: TimeUnit, toUnit: TimeUnit): number {
	let timeInSec: number;
	switch (fromUnit) {
		case 'sec': timeInSec = value * 1; break;
		case 'min': timeInSec = value * 60; break;
		case 'hr': timeInSec = value * 3600; break;
	}
	switch (toUnit) {
		case 'sec': return timeInSec / 1;
		case 'min': return timeInSec / 60;
		case 'hr': return timeInSec / 3600;
	}
}
