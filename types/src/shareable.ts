import { JsonHelper, JsonObject, JsonType } from 'pine-lib';

/** The permissions that a shareable can have. */
export type Permission = 'none' | 'view' | 'edit' | 'full';

/** The permissions that are returned to a user with 'full' access to a shareable. */
export type Permissions = {

	/** The permissions for individual users. If the key is '*', then it means all users. */
	permissions: Record<string, Permission>,

	/** If a string password, it is viewable by anyone that has the password.
	 * If it is false, it overrides any inherited publicViewPassword and forbids public viewing.
	 * If undefined, it inherits any folder above. */
	publicViewPassword: string | false | undefined;
}

export type ShareableBase = {

	/** The name displayed in the folder. */
	name: string,

	/** The type of shareable. */
	type: string,

	/** The content of the shareable. */
	content: JsonObject
}

/** A shareable that is sent to the user. */
export type ShareableForUser = ShareableBase & {

	/** The permission of the shareable. */
	permission: Exclude<Permission, 'none'>
}

/** A shareable that is on the file system. */
export type Shareable = ShareableBase & Permissions;

/** A list of shareables. */
export type FolderItem = {
	name: string,
	type: string,
	isLink: boolean,
	starred: boolean
};

/** A shortcut that links to another folder. */
export type FolderLinkContent = {

	/** The path where the actual folder is. */
	path: string,
}

/** A shortcut that links to another file. */
export type LinkContent = {

	/** The path where the actual file is. */
	path: string,

	/** The type of the actual file. */
	type: string
}

/** A folder that contains other shareables. */
export type FolderContent = {

	/** The list of files and folders. Sorted first by starred, then alphabetically by name. */
	list: FolderItem[]
};

/** A tree of all of the folders. It exists at `<username>/__.tree.json`. */
export type FolderTree = {
	name: string,
	list: FolderTree
}[];

export const newFolderContent: FolderContent = {
	list: []
};

export const newLinkContent: LinkContent = {
	path: '',
	type: ''
};

/** Verify that the JSON is a shareable. */
export function isShareable(json: JsonType | undefined): json is Shareable {
	return JsonHelper.isObject(json)
		&& JsonHelper.hasStringProp(json, 'name')
		&& JsonHelper.hasStringProp(json, 'type')
		&& JsonHelper.hasProp(json, 'content', JsonHelper.isObject)
		&& JsonHelper.hasProp(json, 'permissions', (json): json is Record<string, Permission> => JsonHelper.isObjectOfV(json, (value): value is 'view' | 'edit' | 'full' => value === 'none' || value === 'view' || value === 'edit' || value === 'full'))
		&& (json['publicView'] === undefined || json['publicView'] === 'none' || JsonHelper.hasProp(json, 'publicView', (json): json is { salt: string, hash: string } => JsonHelper.isObject(json) && JsonHelper.hasStringProp(json, 'salt') && JsonHelper.hasStringProp(json, 'hash')));
}

/** Verify that the JSON is a shareable folder. */
export function isFolder(json: ShareableBase): json is ShareableBase & { type: 'folder', content: FolderContent } {
	return json.type === 'folder' && JsonHelper.isArrayOfV(json.content['list'], (v): v is JsonObject => {
		return JsonHelper.isObject(v)
			&& JsonHelper.hasStringProp(v, 'name')
			&& JsonHelper.hasStringProp(v, 'type')
			&& JsonHelper.hasBooleanProp(v, 'isLink')
			&& JsonHelper.hasBooleanProp(v, 'starred');
	});
}

/** Verify that the JSON is a shareable link. */
export function isLink(json: ShareableBase): json is ShareableBase & { type: 'link', content: LinkContent } {
	return json.type === 'link' && JsonHelper.hasStringProp(json.content, 'path') && JsonHelper.hasStringProp(json.content, 'type');
}
