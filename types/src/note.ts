export type NoteContent = {
	text: string;
}

export const newNoteContent: NoteContent = {
	text: ''
};
